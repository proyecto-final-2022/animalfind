package es.bgonzalez.animalfind.security;

import java.util.ArrayList;
import java.util.List;

import es.bgonzalez.animalfind.data.entity.User;
import es.bgonzalez.animalfind.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("No user present with email: " + email);
        } else {
            return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getHashedPassword(),
                    getAuthorities(user));
        }
    }

    private static List<GrantedAuthority> getAuthorities(User user) {
        List<GrantedAuthority> permisos = new ArrayList<>();
        if (user.getRol().getName().equalsIgnoreCase("USER")){
            permisos.add(new SimpleGrantedAuthority("ROLE_USER"));
        } else if(user.getRol().getName().equalsIgnoreCase("ADMIN")){
            permisos.add(new SimpleGrantedAuthority("ROLE_USER"));
            permisos.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        } else{
            return null;
        }

        return permisos;

    }

}
