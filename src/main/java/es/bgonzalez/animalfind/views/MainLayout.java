package es.bgonzalez.animalfind.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.Nav;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.dom.ThemeList;
import com.vaadin.flow.router.HighlightConditions;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.auth.AccessAnnotationChecker;
import com.vaadin.flow.theme.lumo.Lumo;
import es.bgonzalez.animalfind.data.entity.User;
import es.bgonzalez.animalfind.security.AuthenticatedUser;
import es.bgonzalez.animalfind.views.pages.home.HomeView;
import es.bgonzalez.animalfind.views.pages.user.animals.MyAnimalsView;
import es.bgonzalez.animalfind.views.pages.user.booking.BookingView;
import es.bgonzalez.animalfind.views.pages.user.profile.EditProfileView;
import es.bgonzalez.animalfind.views.pages.user.saves.SavesView;

import java.util.Optional;

/**
 * Esta vista principal es la que se muestra encima de las otras vistas.
 */
@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class MainLayout extends AppLayout {
    //Variables.
    private H1 viewTitle;
    private final AuthenticatedUser authenticatedUser;
    private final AccessAnnotationChecker accessChecker;
    private final Optional<User> maybeUser;


    //Constructores.
    /**
     * Constructor por parametros que recibe la identification del usuario y los permisos.
     * @param authenticatedUser usuario activo.
     * @param accessChecker permisos del usuario activo.
     */
    public MainLayout(AuthenticatedUser authenticatedUser, AccessAnnotationChecker accessChecker) {
        this.authenticatedUser = authenticatedUser;
        this.accessChecker = accessChecker;
        this.maybeUser = authenticatedUser.get();

        //Pinta la pagina
        setPrimarySection(Section.DRAWER);
        addToNavbar(true, createHeaderContent());
        addToDrawer(createDrawerContent());
    }


    //Métodos de creación de la vista.
    /**
     * Método que devuelve la cabecera.
     * @return componente de la cabecera.
     */
    private HorizontalLayout createHeaderContent() {
        //Cabecera a devolver.
        HorizontalLayout header = new HorizontalLayout();
        header.addClassNames("view-header");

        //Componentes de la cabecera.
        HorizontalLayout left = new HorizontalLayout();
            DrawerToggle toggle = new DrawerToggle();
            toggle.addClassNames("view-toggle");
            toggle.addThemeVariants(ButtonVariant.LUMO_CONTRAST);
            toggle.getElement().setAttribute("aria-label", "Menu toggle");
            viewTitle = new H1();
            viewTitle.addClassNames("view-title");
        left.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        left.add(toggle, viewTitle);

        HorizontalLayout right = new HorizontalLayout();
            Button toggleButton = new Button("Tema dia | Tema noche", click -> {
                ThemeList themeList = UI.getCurrent().getElement().getThemeList(); // (1)

                if (themeList.contains(Lumo.DARK)) { // (2)
                    themeList.remove(Lumo.DARK);
                } else {
                    themeList.add(Lumo.DARK);
                }
            });
            toggleButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        right.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        right.add(toggleButton);

        header.add(left, right);
        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        header.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        header.setAlignItems(FlexComponent.Alignment.CENTER);
        header.setWidth("100%");
        header.addClassNames("py-0", "px-m");
        return header;
    }
    /**
     * Método que crea el menu y su contenido.
     * @return menu de navegación.
     */
    private Component createDrawerContent() {
        //Titulo del menu.
        H2 appName = new H2("AnimalFind");
        appName.addClassNames("app-name");

        //Añadido de titulo, cuerpo y pie del menu.
        com.vaadin.flow.component.html.Section section = new com.vaadin.flow.component.html.Section(appName,
                createNavigation(), createFooter());
        section.addClassNames("drawer-section");
        return section;
    }
    /**
     * Método que crea el cuerpo del menu.
     * @return navegación.
     */
    private Nav createNavigation() {
        //Creación de la navegación.
        Nav nav = new Nav();
        nav.addClassNames("menu-item-container");
        nav.getElement().setAttribute("aria-labelledby", "views");

        //Soltar los links en la lista para que se pinten.
        UnorderedList list = new UnorderedList();
        list.addClassNames("navigation-list");
        nav.add(list);

        //Añadido de los links a la lista.
        for (MenuItemInfo menuItem : createMenuItems()) {
            if (accessChecker.hasAccess(menuItem.getView())) {
                list.add(menuItem);
            }

        }
        return nav;
    }
    /**
     * Método que crear un pie de página dependiendo si un usuario está registrado o no.
     * @return pie de la pagina.
     */
    private Footer createFooter() {
        Footer layout = new Footer();
        layout.addClassNames("footer");

        //Identifica si existe usuario.
        if (maybeUser.isPresent()) {
            //Recogida de datos a mostrar del usuario
            User user = maybeUser.get();
                Avatar avatar = new Avatar(user.getUsername(), user.getProfilePictureUrl());
                avatar.addClassNames("me-xs");
                Span name = new Span(user.getUsername());
                name.addClassNames("font-medium", "text-s", "text-secondary");


            ContextMenu userMenu = new ContextMenu(avatar);
                userMenu.setOpenOnClick(true);
                userMenu.addItem("Logout", e -> authenticatedUser.logout());
            ContextMenu userMenu2 = new ContextMenu(name);
            userMenu2.setOpenOnClick(true);
            userMenu2.addItem("Logout", e -> authenticatedUser.logout());

            layout.add(avatar, name);
        } else {
            HorizontalLayout sessionOptions = new HorizontalLayout();
            Anchor loginLink = new Anchor("login", "Iniciar sesión");
            Anchor registerLink = new Anchor("register", "Registrate");
            sessionOptions.add(loginLink, registerLink);

            layout.add(sessionOptions);
        }

        return layout;
    }

    //Sub-métodos.
    /**
     * Método que crear una lista con los items de las opciones de navegación.
     * @return lista con las opciones.
     */
    private MenuItemInfo[] createMenuItems() {
        return new MenuItemInfo[]{ //
                new MenuItemInfo("Casa", "la la-home", HomeView.class), //
                new MenuItemInfo("Editar Perfil", "la la-user", EditProfileView.class), //
                new MenuItemInfo("Mis animales", "la la-tree", MyAnimalsView.class), //
                new MenuItemInfo("Guardados", "la la-bookmark", SavesView.class), //
                new MenuItemInfo("Reservados", "la la-bookmark", BookingView.class), //
        };
    }
        /**
         * Simple componente de item de navegación, que extiende de LisItem.
         */
        private static class MenuItemInfo extends ListItem {

            private final Class<? extends Component> view;

            /**
             * Constructor del item de navegación.
             * @param menuTitle titulo que aparece en el menu.
             * @param iconClass icono que aparece al lado del título.
             * @param view vista a mostrar.
             */
            public MenuItemInfo(String menuTitle, String iconClass, Class<? extends Component> view) {
                //Asignación de la vista a un direccionamiento.
                this.view = view;
                RouterLink link = new RouterLink();
                link.setHighlightCondition(HighlightConditions.sameLocation());
                link.addClassNames("menu-item-link");
                link.setRoute(view);

                //Asignación del título.
                Span text = new Span(menuTitle);
                text.addClassNames("menu-item-text");

                //Asignación icono con el método creado para ello y título al direccionamiento, y añadido de este al componente.
                link.add(new LineAwesomeIcon(iconClass), text);
                add(link);
            }

            /**
             * Método que devuelve la vista.
             * @return vista.
             */
            public Class<?> getView() {
                return view;
            }

            /**
             * Envoltura simple para crear los iconos usando LineAwesome icon-set. Mirar
             * <a href="https://icons8.com/line-awesome">https://icons8.com/line-awesome</a>
             */
            @NpmPackage(value = "line-awesome", version = "1.3.0")
            public static class LineAwesomeIcon extends Span {
                /**
                 * Constructor que recibe un nombre a buscar de icono y lo asigna.
                 * @param lineawesomeClassnames nombre del icono a buscar.
                 */
                public LineAwesomeIcon(String lineawesomeClassnames) {
                    addClassNames("menu-item-icon");
                    if (!lineawesomeClassnames.isEmpty()) {
                        addClassNames(lineawesomeClassnames);
                    }
                }
            }

        }

    /**
     * Método que utiliza getCurrentPageTitle() para asignarlo después de navegar a la nueva vista.
     */
    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        viewTitle.setText(getCurrentPageTitle());
    }
        /**
         * Método que recupera el nombre la vista actual.
         * @return nombre de la vista actual.
         */
        private String getCurrentPageTitle() {
            PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
            return title == null ? "" : title.value();
        }
}
