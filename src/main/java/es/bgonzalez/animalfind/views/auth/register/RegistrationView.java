package es.bgonzalez.animalfind.views.auth.register;


import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import es.bgonzalez.animalfind.data.service.AnimalfindService;
import es.bgonzalez.animalfind.security.AuthenticatedUser;
import es.bgonzalez.animalfind.views.MainLayout;
import es.bgonzalez.animalfind.views.pages.home.HomeView;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Clase que pinta la pagina de registro.
 */
@Component
@Scope("prototype")
@PageTitle("Registrate!")
@Route(value = "register", layout = MainLayout.class)
@RouteAlias(value = "register", layout = MainLayout.class)
@AnonymousAllowed
public class RegistrationView extends VerticalLayout implements BeforeEnterObserver {
    //Atributos
    private final AuthenticatedUser authenticatedUser;


    //Constructores.
    /**
     * Constructor por parametros para poder realizar el alta de un usuario nuevo.
     * @param authenticatedUser usuario activo.
     * @param service servicio de back-end.
     * @param passwordEncoder encriptado.
     */
    public RegistrationView(AuthenticatedUser authenticatedUser, AnimalfindService service, PasswordEncoder passwordEncoder){
        this.authenticatedUser =  authenticatedUser;

        //Creación de las validaciones.
        RegistrationForm registrationForm = new RegistrationForm();
        RegistrationFormBinder registrationFormBinder = new RegistrationFormBinder(registrationForm);
        registrationFormBinder.addBindingAndValidation(service, passwordEncoder);

        //Ajuste de la vista.
        setHorizontalComponentAlignment(Alignment.CENTER, registrationForm);
        add(registrationForm);
    }


    //Métodos funcionales.
    /**
     * Método para validar que el usuario no es activo, y que no está registrado.
     * @param event comprobación antes de entrar.
     */
    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if(authenticatedUser.get().isPresent()){
            Notification.show("No puede realizar esta acción. No esta registrado.").addThemeVariants(NotificationVariant.LUMO_ERROR);
            event.forwardTo(HomeView.class);
        }
    }
}
