package es.bgonzalez.animalfind.views.auth.login;

import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

/**
 * Clase que es la vista del login.
 */
@PageTitle("Login")
@Route(value = "login")
public class LoginView extends LoginOverlay {
    private LoginI18n i18n;

    public LoginI18n getI18n() {
        return i18n;
    }
//Constructores.
    /**
     * Constructor por defecto de la vista.
     */
    public LoginView() {
        //Identificar que este será el login y su creación.
        setAction("login");
        LoginI18n i18n = LoginI18n.createDefault();
        //Obtención del formulario y manipulado de este.
            i18n.getForm().setUsername("Correo electronico");
            i18n.getForm().setTitle("Iniciar sesión");
            i18n.getForm().setPassword("Contraseña");

            //Cabecera
            i18n.setHeader(new LoginI18n.Header());
            i18n.getHeader().setTitle("AnimalFind");
            i18n.getHeader().setDescription("Esto esta una app de prueba, Credenciales: user@animalfind.es/User-123");
            i18n.setAdditionalInformation("Por favor, contacta con admin@animalfind.es si estas experimentando fallos al intentar logearte.");

            //Mensaje de error.
            i18n.getErrorMessage().setTitle("Correo electronico o contraseña incorrecta.");
            i18n.getErrorMessage().setMessage("Revisa que estas introduciendo el correo electronico y contraseña correctas e intentalo de nuevo.");

        //Métodos funcionales.
        setOpened(true);
        setForgotPasswordButtonVisible(false);

        setI18n(i18n);
    }
}
