package es.bgonzalez.animalfind.views.auth.register;

import com.vaadin.flow.component.HasValueAndElement;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;

import java.util.stream.Stream;

/**
 * Clase que crea la vista del formulario de registro.
 */
public class RegistrationForm extends FormLayout {
    //Variables.
    private final TextField username = new TextField("Nombre de usuario");
    private final EmailField email = new EmailField("Correo electronico");
    private final PhoneNumberField phone = new PhoneNumberField("Numero de teléfono");
    private final PasswordField password = new PasswordField("Contraseña");
    private final PasswordField confirmPassword = new PasswordField("Confirmar contraseña");
    private final Button registerButton = new Button("Registrarse");
    private final Span errorMessageField;


    //Constructor
    /**
     * Constructor por defecto que realiza la forma del formulario de alta a un usuario.
     */
    public RegistrationForm(){
        //Cabecera
        H3 title = new H3("Registrate");
        //Cuerpo
        setRequiredIndicatorVisible(username, email, password, confirmPassword, phone);
        errorMessageField = new Span();

        //Atributos
        add(title, username, email, password, confirmPassword, phone, errorMessageField, registerButton);
        setMaxWidth("500px");
        setColspan(username, 2);
        setColspan(email, 2);
        setColspan(phone, 2);
        setColspan(registerButton, 2);

    }


    //Getters and setters.


    public TextField getUsername() {
        return username;
    }

    public EmailField getEmail() {
        return email;
    }

    public PhoneNumberField getPhone() {
        return phone;
    }

    /**
     * Método que obtiene el field de contraseña.
     * @return contraseña.
     */
    public PasswordField getPassword() {
        return password;
    }
    /**
     * Método que obtiene el field de contraseña confirmada.
     * @return contraseña confirmada.
     */
    public PasswordField getConfirmPassword() {
        return confirmPassword;
    }
    /**
     * Método que obtiene el botón de registrarse.
     * @return botón de registrarse.
     */
    public Button getRegisterButton() {
        return registerButton;
    }
    /**
     * Método que obtiene el error.
     * @return mensaje de error.
     */
    public Span getErrorMessageField() {
        return errorMessageField;
    }


    //Métodos funcionales.
    /**
     * Método que muestra que es requirido un componente.
     * @param components componentes a asignar.
     */
    private void setRequiredIndicatorVisible(HasValueAndElement<?, ?>... components) {
        Stream.of(components).forEach(comp -> comp.setRequiredIndicatorVisible(true));
    }
    /**
     * Realizar una field personalizada, en este caso para un teléfono.
     */

    //Clases.
    public static class PhoneNumberField extends CustomField<String> {
        private final ComboBox<String> countryCode = new ComboBox<>();
        private final TextField number = new TextField();

        public PhoneNumberField(String label) {
            setLabel(label);
            countryCode.setWidth("120px");
            countryCode.setPlaceholder("Country");
            countryCode.setPattern("\\+\\d*");
            countryCode.setPreventInvalidInput(true);
            countryCode.setItems("+33", "+34");
            countryCode.addCustomValueSetListener(e -> countryCode.setValue(e.getDetail()));
            number.setPattern("\\d*");
            number.setPreventInvalidInput(true);
            HorizontalLayout layout = new HorizontalLayout(countryCode, number);
            layout.setFlexGrow(1.0, number);
            add(layout);
        }

        @Override
        protected String generateModelValue() {
            if (countryCode.getValue() != null && number.getValue() != null) {
                return countryCode.getValue() + " " + number.getValue();
            }
            return "";
        }

        @Override
        protected void setPresentationValue(String phoneNumber) {
            String[] parts = phoneNumber != null ? phoneNumber.split(" ", 2) : new String[0];
            if (parts.length == 1) {
                countryCode.clear();
                number.setValue(parts[0]);
            } else if (parts.length == 2) {
                countryCode.setValue(parts[0]);
                number.setValue(parts[1]);
            } else {
                countryCode.clear();
                number.clear();
            }
        }

        @Override
        public String toString() {
            return countryCode.getValue() + " " + number.getValue();
        }
    }
}
