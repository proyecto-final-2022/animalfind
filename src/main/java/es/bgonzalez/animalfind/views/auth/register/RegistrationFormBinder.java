package es.bgonzalez.animalfind.views.auth.register;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.server.VaadinServletRequest;
import es.bgonzalez.animalfind.data.entity.User;
import es.bgonzalez.animalfind.data.service.AnimalfindService;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

/**
 * Clase que sirve para poder añadir y validar a un usuario si es posible.
 */
public class RegistrationFormBinder {
    //Atributos.
    private final RegistrationForm registrationForm;
    private boolean enablePasswordValidation;
    private User user;
    private BeanValidationBinder<User> binder;


    //Constructor.
    /**
     * Constructor para el intento de registrarse.
     * @param registrationForm formulario de usuario de registro.
     */
    public RegistrationFormBinder(RegistrationForm registrationForm) {
        this.registrationForm = registrationForm;
    }


    //Métodos funcionales.
    /**
     * Método que añade el bean con los campos nuevos y no nuevos del usuario activo para validarlo, si es correcto
     * lo crea.
     * @param service servicio para actualizar en el backend.
     * @param passwordEncoder cifrado de la nueva contraseña.
     */
    @Bean
    public void addBindingAndValidation(AnimalfindService service, PasswordEncoder passwordEncoder){
        if(binder == null){
            binder = new BeanValidationBinder<>(User.class);
        }
        binder.bindInstanceFields(registrationForm);

        //Validación personalizada por campo.
        binder.forField(registrationForm.getPassword())
                .withValidator(this::passwordValidator).bind("hashedPassword");

        //La confirmación no está conectada con binder, pero queremos que binder
        //haga un re-check del validador de contraseñas cunado el field cambie
        //de valor. El camino más fácil es hacerlo manual.
        registrationForm.getConfirmPassword().addValueChangeListener(e -> {
            //El usuario ha modificado el segundo campo, entonces podemos validarlo.
            // visite passwordValidator() para ver como funciona esto.
            enablePasswordValidation = true;
            binder.validate();
        });

        //Muestra el label cuando existe un error bean.
        binder.setStatusLabel(registrationForm.getErrorMessageField());

        //Finalmente, el botón de registrarse.
        registrationForm.getRegisterButton().addClickListener( event -> {
            try {
                //Crear un bean vacio de fabrica con detalles.
                if(user == null){
                    user = new User();
                }
                //Iniciar validadores y escribir los valores del bean.
                binder.writeBean(user);

                //Llamada al backend.
                //Asignación de rol usuario.
                //Contraseña encriptada.
                user.setRol(service.RolFindByName("USER"));
                user.setHashedPassword(passwordEncoder.encode(user.getHashedPassword()));
                service.userCreate(user);

                //Mostrar mensajes de que salió como queríamos.
                showSuccess(user);

            } catch (ValidationException ignored) {
            }
        });
    }
    /**
     * Método que valida la contraseña, en específico la longitud y que coincidan.
     * @param s contraseña a validar
     * @param valueContext contexto.
     * @return si la contraseña es validad o no.
     */
    private ValidationResult passwordValidator(String s, ValueContext valueContext) {
        //Verifica que la contraseña no es nula y tenga al menos 8 digitos.
        assert s != null;
        if(s.length() < 8){
            return  ValidationResult.error("La contraseña tiene que tener un minima de 8 de longitud.");
        }

        //El usuario no ha rellenado la confirmación, asi que no valide todavía.
        if(!enablePasswordValidation){
            enablePasswordValidation = true;
            return ValidationResult.ok();
        }

        //Verifica que los dos campos de contraseña sean iguales.
        String s2 = registrationForm.getConfirmPassword().getValue();
        if(s.equals(s2)){
            return ValidationResult.ok();
        }
        return ValidationResult.error("Las contraseñas no coinciden");
    }
    /**
     * Método que es muestra una notificación y redirecciona.
     * @param user usuario que es dado de alta.
     */
    private void showSuccess(User user){
        //Notificación a mostrar.
        Notification notification =
                Notification.show("Datos guardados, bienvenido " + user.getUsername());
        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);

        //Redireccionamiento.
        UI.getCurrent().getPage().setLocation("login");
        SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
        logoutHandler.logout(VaadinServletRequest.getCurrent().getHttpServletRequest(), null, null);
    }

    public void setUser(User user) {
        this.user = user;
        if(binder == null){
            binder = new BeanValidationBinder<>(User.class);
        }
        binder.readBean(user);
    }

    public RegistrationForm getRegistrationForm() {
        return registrationForm;
    }
}
