package es.bgonzalez.animalfind.views.utils;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.entity.Save;
import es.bgonzalez.animalfind.data.service.AnimalfindService;
import es.bgonzalez.animalfind.security.AuthenticatedUser;

public class ViewCard extends ListItem {
    private AuthenticatedUser authenticatedUser;
    private AnimalfindService animalfindService;
    private String photo;
    private String title;
    private String type;
    private String breed;
    private String age;
    private String description;
    private String contact;
    private String phone;
    private boolean withBotton;
    private Animal animal;

    public ViewCard(AuthenticatedUser authenticatedUser, AnimalfindService animalfindService, Animal animal) {
        this.authenticatedUser = authenticatedUser;
        this.animalfindService = animalfindService;
        this.animal = animal;

        this.photo = animal.getImagenProfile();
        this.title = animal.getName();

        this.type = "Tipo: " + animal.getAnimal_type().getName();
        this.breed = "Raza: " + animal.getBreed();
        this.age = "Edad: " + String.valueOf(animal.getAge());

        this.description = animal.getDescription();

        this.contact = "Dueño: " + animal.getUser().getUsername();
        this.phone = "Telefono: " + animal.getUser().getPhone();
        this.withBotton = true;
        createViewCard();
    }

    private void createViewCard() {
        addClassNames("bg-contrast-5", "flex", "flex-col", "items-start", "p-m", "rounded-l");
        VerticalLayout layout = new VerticalLayout();

        Div div = new Div();
        div.addClassNames("bg-contrast", "flex items-center", "justify-center", "mb-m", "overflow-hidden",
                "rounded-m w-full");
        div.setHeight("160px");

        Image image = new Image();
        image.setWidth("100%");
        image.setSrc(this.photo);
        div.add(image);

        Span header = new Span();
        header.addClassNames("text-xl", "font-semibold");
        header.setText(this.title);

        Span type = new Span();
        type.addClassNames("text-s", "text-secondary");
        type.setText(this.type);

        Span breed = new Span();
        breed.addClassNames("text-s", "text-secondary");
        breed.setText(this.breed);

        Span age = new Span();
        age.addClassNames("text-s", "text-secondary");
        age.setText(this.age);

        TextArea description = new TextArea();
        description.setWidthFull();
        description.setMinHeight("100px");
        description.setMaxHeight("150px");
        description.setLabel("Description");
        description.setValue(this.description);
        description.addClassName("my-m");
        description.setReadOnly(true);

        Span contact = new Span();
        contact.addClassNames("text-s", "text-secondary");
        contact.setText(this.contact);

        Span phone = new Span();
        phone.addClassNames("text-s", "text-secondary");
        phone.setText(this.phone);

        layout.add(div, header, type, breed, age, description, contact, phone);
        add(layout);

        if(authenticatedUser.get().isPresent()){
            Span badge = new Span();
            badge.getElement().setAttribute("theme", "badge");
            Button buttonOption = new Button();
                if(animalfindService.saveSearchSaveFromUser(authenticatedUser.get().get(), animal) != null){
                    buttonOption.setText("Eliminar");
                    buttonOption.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
                    buttonOption.setDisableOnClick(true);
                    buttonOption.addClickListener(event -> {
                        deleteSave();
                        Notification.show("Animal eliminado de la lista correctamente.").addThemeVariants(NotificationVariant.LUMO_PRIMARY);
                        //UI.getCurrent().getPage().reload();
                    });
                } else{
                    buttonOption.setText("Guardar");
                    buttonOption.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                    buttonOption.setDisableOnClick(true);
                    buttonOption.addClickListener(event -> {
                        saveSave();
                        Notification.show("Animal añadido de la lista correctamente.").addThemeVariants(NotificationVariant.LUMO_PRIMARY);
                        //UI.getCurrent().getPage().reload();
                    });
                }
            badge.add(buttonOption);
            add(badge);
        }
    }

    private void saveSave() {
        Save newSave = new Save();
        newSave.setUser(authenticatedUser.get().get());
        newSave.setAnimal(animal);
        animalfindService.saveCreate(newSave);
    }

    private void deleteSave() {
        animalfindService.saveDeleteAnimalFromUser(authenticatedUser.get().get(), this.animal);
    }
}
