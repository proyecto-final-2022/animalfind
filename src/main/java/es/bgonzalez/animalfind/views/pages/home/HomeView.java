package es.bgonzalez.animalfind.views.pages.home;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.entity.AnimalType;
import es.bgonzalez.animalfind.data.entity.User;
import es.bgonzalez.animalfind.data.service.AnimalfindService;
import es.bgonzalez.animalfind.security.AuthenticatedUser;
import es.bgonzalez.animalfind.views.MainLayout;
import es.bgonzalez.animalfind.views.auth.login.LoginView;
import es.bgonzalez.animalfind.views.auth.register.RegistrationView;
import es.bgonzalez.animalfind.views.pages.user.animals.MyAnimalsView;
import es.bgonzalez.animalfind.views.utils.ViewCard;

import java.util.List;
import java.util.Optional;

/**
 * Clase que pinta la vista por defecto al cargar la aplicación.
 */
@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
@PageTitle("Home")
@Route(value = "home", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
@AnonymousAllowed
public class HomeView extends VerticalLayout implements HasComponents, HasStyle {
    //Variables.
    private final AuthenticatedUser authenticatedUser;
    private final AnimalfindService animalfindService;
    private final Optional<User> maybeUser;
    private OrderedList imageContainer;
    private TextField searchField;
    private Select<AnimalType> sortBy;


    //Constructores.
    /**
     * Constructor por parametros que identifica al usuario y utiliza los servicios del back-end.
     * @param authenticatedUser usuario activo.
     * @param animalfindService servicios back-end.
     */
    public HomeView(AuthenticatedUser authenticatedUser, AnimalfindService animalfindService) {
        this.authenticatedUser = authenticatedUser;
        this.animalfindService = animalfindService;
        this.maybeUser = authenticatedUser.get();

        //Pintado de la vista.
        addClassNames("home-view", "max-w-screen-lg", "mx-auto", "pb-l", "px-l");
        add(createNavBar(), createHeader(), createAnimalsShow());
    }


    //Métodos que pintan la vista.
    /**
     * Método que crea la barra de navegación de la vista.
     * @return la navegación de la pagina.
     */
    private Component createNavBar() {
        //Cabecera a devolver.
        Header header = new Header();
        header.addClassNames("view-header");
        //Diseño de la cabecera y creación de sus componentes.
            HorizontalLayout designHeader = new HorizontalLayout();
            designHeader.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
            designHeader.setAlignItems(FlexComponent.Alignment.CENTER);
            designHeader.setSizeFull();
            designHeader.setPadding(true);
                //Componente barra de búsqueda.
                searchField = new TextField();
                searchField.setPlaceholder("Busca por nombre, raza...");
                searchField.setPrefixComponent(VaadinIcon.SEARCH.create());
                searchField.setWidth("50%");
                searchField.setClearButtonVisible(true);
                searchField.setValueChangeMode(ValueChangeMode.LAZY);
                searchField.addValueChangeListener(e -> updateList());

                //Componente que tiene los botones.
                HorizontalLayout buttons = new HorizontalLayout();
                    //Componente para subir.
                    Button uploadButton = new Button("Subir");
                    uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                        if (maybeUser.isPresent()) {
                            uploadButton.addClickListener(event -> UI.getCurrent().navigate(MyAnimalsView.class));
                        } else{
                            uploadButton.addClickListener(event -> {
                                            Notification.show("¿Ya eres usuario?, logeate y sube tu animal!").addThemeVariants(NotificationVariant.LUMO_PRIMARY);
                                            UI.getCurrent().navigate(LoginView.class);
                                        });
                        }
                    buttons.add(uploadButton);
                    //Componente de opciones de sesión.
                    if (maybeUser.isPresent()) {
                        Button logoutButton = new Button("Cerrar sesión");
                        logoutButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                        logoutButton.addClickListener(event -> authenticatedUser.logout());
                        buttons.add(logoutButton);
                    } else{
                        Button singUpButton = new Button("Iniciar sesión");
                        singUpButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                        singUpButton.addClickListener(event -> UI.getCurrent().navigate(LoginView.class));
                        Button registerButton = new Button("Registrarse");
                        registerButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
                        registerButton.addClickListener(event -> UI.getCurrent().navigate(RegistrationView.class));
                        buttons.add(singUpButton, registerButton);

                    }
            designHeader.add(searchField, buttons);

        header.add(designHeader);
        return header;
    }
    /**
     * Método que crea la cabecera de la vista.
     * @return cabecera de la vista.
     */
    private HorizontalLayout createHeader() {
        //Cabecera a devolver.
        HorizontalLayout container = new HorizontalLayout();
        container.addClassNames("items-center", "justify-between");

        //Parte de la derecha
        VerticalLayout headerContainer = new VerticalLayout();
                H2 header = new H2("Animales disponibles");
                header.addClassNames("mb-0", "mt-xl", "text-3xl");
                Paragraph description = new Paragraph("Todos están esperando a alguien, tu puedes ser el siguiente!");
                description.addClassNames("mb-xl", "mt-0", "text-secondary");
        headerContainer.add(header, description);

        //Parte de la izquierda.
        HorizontalLayout filters = new HorizontalLayout();
        filters.setAlignItems(FlexComponent.Alignment.END);
            sortBy = new Select<>();
            sortBy.setLabel("Filtrar por");
            sortBy.setPlaceholder("Filtro");
            sortBy.setItems(animalfindService.AnimalTypesFindAll());
            sortBy.setItemLabelGenerator(AnimalType::getName);
            sortBy.addValueChangeListener(event -> updateList());

            Button clearSort = new Button("Limpiar filtros");
            clearSort.addClickListener(event -> {
                updateList();
                sortBy.setValue(null);
            });
        filters.add(sortBy, clearSort);

        //Añadidos de estas.
        container.add(headerContainer, filters);
        return container;
    }
    /**
     * Método que crea los animales que se muestran.
     * @return animales a mostrar.
     */
    private OrderedList createAnimalsShow() {
        imageContainer = new OrderedList();
        imageContainer.addClassNames("gap-m", "grid", "list-none", "m-0", "p-0");
        updateList();
        return imageContainer;
    }


    //Métodos funcionales.
    /**
     * Método que actualiza la lista que se muestra.
     */
    private void updateList() {
        imageContainer.removeAll();
        List<Animal> animalList = animalfindService.animalFindAll(searchField.getValue(), sortBy.getValue());
        for (Animal a: animalList) {
            if(a.getStatus().equals("Disponible")){
                imageContainer.add(new ViewCard(authenticatedUser, animalfindService, a));
            }
        }
    }
}