package es.bgonzalez.animalfind.views.pages.user.booking;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.entity.Booking;
import es.bgonzalez.animalfind.data.service.AnimalfindService;
import es.bgonzalez.animalfind.security.AuthenticatedUser;
import es.bgonzalez.animalfind.views.MainLayout;

import javax.annotation.security.PermitAll;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que crea la vista de los animales guardados.
 */
@SuppressWarnings("OptionalGetWithoutIsPresent")
@PageTitle("Reservados")
@Route(value = "booking", layout = MainLayout.class)
@RouteAlias(value = "booking", layout = MainLayout.class)
@PermitAll
public class BookingView extends VerticalLayout {
    //Variables.
    private final AuthenticatedUser authenticatedUser;
    private final AnimalfindService animalfindService;
    private Grid<Animal> grid;
    private ComboBox<Animal> animals;


    //Constructores.
    /**
     * Constructor de la vista.
     * @param authenticatedUser usuario activo.
     * @param animalfindService servicios del back-end.
     */
    public BookingView(AuthenticatedUser authenticatedUser, AnimalfindService animalfindService) {
        this.authenticatedUser = authenticatedUser;
        this.animalfindService = animalfindService;
        addClassName("list-view");
        setSizeFull();

        animals = new ComboBox<>("Animales en reserva");
        animals.setItemLabelGenerator(Animal::getName);
        add(animals);

        configureGrid();
        updateListAnimals();

    }


    //Métodos de vista.
    /**
     * Método que configura el grid con sus columnas para mostrar los animales guardados del usuario activo y poder eliminarlos.
     */
    private void configureGrid() {
        grid = new Grid<>(Animal.class, false);
        grid.addColumn(createAvatarRenderer()).setHeader("Image").setAutoWidth(true).setFlexGrow(0).setSortable(true);
        grid.addColumn(Animal::getName).setHeader("Nombre").setSortable(true);
        grid.addColumn(Animal-> Animal.getAnimal_type().getName()).setHeader("Tipo").setSortable(true);
        grid.addColumn(Animal-> Animal.getUser().getUsername()).setHeader("Dueño").setSortable(true);
        add(grid);
    }
        /**
         * Sub-método para poder mostrar la imagen del animal.
         * @return imagen.
         */
        private static Renderer<Animal> createAvatarRenderer() {
            return LitRenderer.<Animal>of("<vaadin-avatar img=\"${item.pictureUrl}\" name=\"${item.fullName}\" alt=\"User avatar\"></vaadin-avatar>")
                    .withProperty("pictureUrl", Animal::getImagenProfile);
        }


    //Métodos funcionales.
    /**
     * Método que recarga la lista que es mostrada en la vista de los animales guardados del usuario activo.
     */
    private void updateListAnimals() {
        List<Animal> animalList = new ArrayList<>();
        for(Animal a: animalfindService.animalFindByUser(authenticatedUser.get().get())){
            if (a.getStatus().equals("En reserva")){
                animalList.add(a);
            }
        }
        animals.setItems(animalList);
        grid.setItems(animalList);
    }
    /**
     * Método que elimina de la lista que es mostrada en la vista el animal elegido por el usuario activo.
     * @param animal entidad a eliminar.
     */
    private void deleteFromList(Animal animal) {
        if (animal == null)
            return;
        animalfindService.bookingDeleteAnimalFromUser(authenticatedUser.get().get(), animal);
        updateListAnimals();
    }
}
