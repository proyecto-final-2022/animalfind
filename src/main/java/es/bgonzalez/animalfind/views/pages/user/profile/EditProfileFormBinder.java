package es.bgonzalez.animalfind.views.pages.user.profile;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.server.VaadinServletRequest;
import es.bgonzalez.animalfind.data.entity.User;
import es.bgonzalez.animalfind.data.service.AnimalfindService;
import es.bgonzalez.animalfind.security.AuthenticatedUser;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

/**
 * Clase que sirve para poder actualizar y validar a un usuario si es posible.
 */
@SuppressWarnings("OptionalGetWithoutIsPresent")
public class EditProfileFormBinder {
    //Atributos.
    private final EditProfileForm editProfileForm;
    private boolean enablePasswordValidation;


    //Constructores.
    /**
     * Constructor para el intento de actualizar.
     * @param editProfileForm formulario de usuario existente.
     */
    public EditProfileFormBinder(EditProfileForm editProfileForm) {
        this.editProfileForm = editProfileForm;
    }


    //Métodos funcionales.
    /**
     * Método que añade el bean con los campos nuevos y no nuevos del usuario activo para validarlo, si es correcto
     * lo actualizada.
     * @param authenticatedUser usuario activo.
     * @param service servicio para actualizar en el backend.
     * @param passwordEncoder cifrado de la nueva contraseña.
     */
    @Bean
    public void addBindingAndValidation(AuthenticatedUser authenticatedUser, AnimalfindService service, PasswordEncoder passwordEncoder){
        cargarDatos(authenticatedUser);

        BeanValidationBinder<User> binder = new BeanValidationBinder<>(User.class);
        binder.bindInstanceFields(editProfileForm);

        //Validación personalizada por campo.
        binder.forField(editProfileForm.getPassword())
                .withValidator(this::passwordValidator).bind("hashedPassword");

        //La confirmación no está conectada con binder, pero queremos que binder
        //haga un re-check del validador de contraseñas cunado el field cambie
        //de valor. El camino más fácil es hacerlo manual.
        editProfileForm.getConfirmPassword().addValueChangeListener(e -> {
            //El usuario ha modificado el segundo campo, entonces podemos validarlo.
            // visite passwordValidator() para ver como funciona esto.
            enablePasswordValidation = true;
            binder.validate();
        });

        //Notifica cuando existe un error bean.
        binder.setStatusLabel(editProfileForm.getErrorMessageField());

        //Finalmente, el botón de registrarse.
        editProfileForm.getSavesButton().addClickListener( event -> {
            try {
                //Crear un bean vacio de fabrica con detalles.
                User userBean = authenticatedUser.get().get();
                //Iniciar validadores y escribir los valores del bean.
                binder.writeBean(userBean);

                //Llamada al backend.
                //Contraseña encriptada.
                userBean.setHashedPassword(passwordEncoder.encode(userBean.getHashedPassword()));
                service.userUpdate(userBean);

                //Mostrar mensajes de que salió como queríamos.
                showSuccess(userBean);

            } catch (ValidationException ignored) {
            }
        });

        editProfileForm.getRestartButton().addClickListener(event -> cargarDatos(authenticatedUser));
    }
    /**
     * Método que rellena los datos del formulario con los del usuario.
     * @param authenticatedUser usuario activo.
     */
    private void cargarDatos(AuthenticatedUser authenticatedUser) {
        editProfileForm.getUsername().setValue(authenticatedUser.get().get().getUsername());
        editProfileForm.getEmail().setValue(authenticatedUser.get().get().getEmail());
        editProfileForm.getPassword().setValue("");
        editProfileForm.getPassword().setPlaceholder("Contraseña nueva");
        editProfileForm.getConfirmPassword().setValue("");
        editProfileForm.getConfirmPassword().setPlaceholder("Confirme contraseña");
        editProfileForm.getPhone().setValue(authenticatedUser.get().get().getPhone());
        editProfileForm.getProfilePictureUrl().setValue(authenticatedUser.get().get().getProfilePictureUrl() == null ? "" : authenticatedUser.get().get().getProfilePictureUrl());
        editProfileForm.getImage().setSrc(authenticatedUser.get().get().getProfilePictureUrl() == null ? "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Avatar_icon_green.svg/2048px-Avatar_icon_green.svg.png" : authenticatedUser.get().get().getProfilePictureUrl());
    }
    /**
     * Método que valida la contraseña, en específico la longitud y que coincidan.
     * @param s contraseña a validar
     * @param valueContext contexto.
     * @return si la contraseña es validad o no.
     */
    private ValidationResult passwordValidator(String s, ValueContext valueContext) {
        //Verifica que la contraseña no es nula y tenga al menos 8 dígitos.
        assert s != null;
        if(s.length() < 8){
            return  ValidationResult.error("La contraseña tiene que tener una minima de 8 de largura.");
        }

        //El usuario no ha rellenado la confirmación, asi que no valide todavía.
        if(!enablePasswordValidation){
            enablePasswordValidation = true;
            return ValidationResult.ok();
        }

        //Verifica que los dos campos de contraseña sean iguales.
        String s2 = editProfileForm.getConfirmPassword().getValue();
        if(s.equals(s2)){
            return ValidationResult.ok();
        }
        return ValidationResult.error("Las contraseñas no coinciden");
    }
    /**
     * Método que es muestra una notificación y redirecciona.
     * @param user usuario que es dado de alta.
     */
    private void showSuccess(User user){
        //Notification a mostrar.
        Notification notification =
                Notification.show("Datos actualizados, logeate de nuevo para verificarlos. " + user.getUsername());
        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);

        //Redireccionamiento.
        UI.getCurrent().getPage().setLocation("login");
        SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
        logoutHandler.logout(VaadinServletRequest.getCurrent().getHttpServletRequest(), null, null);
    }

    public EditProfileForm getEditProfileForm() {
        return editProfileForm;
    }
}

