package es.bgonzalez.animalfind.views.pages.user.saves;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.service.AnimalfindService;
import es.bgonzalez.animalfind.security.AuthenticatedUser;
import es.bgonzalez.animalfind.views.MainLayout;

import javax.annotation.security.PermitAll;

/**
 * Clase que crea la vista de los animales guardados.
 */
@SuppressWarnings("OptionalGetWithoutIsPresent")
@PageTitle("Guardados")
@Route(value = "saves", layout = MainLayout.class)
@RouteAlias(value = "saves", layout = MainLayout.class)
@PermitAll
public class SavesView extends VerticalLayout {
    //Variables.
    private final AuthenticatedUser authenticatedUser;
    private final AnimalfindService animalfindService;
    private Grid<Animal> grid;
    private Grid.Column<Animal> deleteColumn;


    //Constructores.
    /**
     * Constructor de la vista.
     * @param authenticatedUser usuario activo.
     * @param animalfindService servicios del back-end.
     */
    public SavesView(AuthenticatedUser authenticatedUser, AnimalfindService animalfindService) {
        this.authenticatedUser = authenticatedUser;
        this.animalfindService = animalfindService;
        addClassName("list-view");
        setSizeFull();
        configureGrid();
        updateListAnimals();
    }


    //Métodos de vista.
    /**
     * Método que configura el grid con sus columnas para mostrar los animales guardados del usuario activo y poder eliminarlos.
     */
    private void configureGrid() {
        grid = new Grid<>(Animal.class, false);
        grid.addColumn(createAvatarRenderer()).setHeader("Image").setAutoWidth(true).setFlexGrow(0).setSortable(true);
        grid.addColumn(Animal::getName).setHeader("Nombre").setSortable(true);
        grid.addColumn(Animal-> Animal.getAnimal_type().getName()).setHeader("Tipo").setSortable(true);
        grid.addColumn(Animal::getBreed).setHeader("Raza").setSortable(true);
        grid.addColumn(Animal::getAge).setHeader("Edad").setSortable(true);
        grid.addColumn(Animal::getStatus).setHeader("Disponibilidad").setSortable(true);
        deleteColumn = grid.addColumn(
                new ComponentRenderer<>(Button::new, (button, animal) -> {
                    button.addThemeVariants(ButtonVariant.LUMO_ICON,
                            ButtonVariant.LUMO_ERROR,
                            ButtonVariant.LUMO_TERTIARY);
                    button.addClickListener(e -> this.deleteFromList(animal));
                    button.setIcon(new Icon(VaadinIcon.TRASH));
                })).setHeader("Manage");
        add(grid);
    }
        /**
         * Sub-método para poder mostrar la imagen del animal.
         * @return imagen.
         */
        private static Renderer<Animal> createAvatarRenderer() {
            return LitRenderer.<Animal>of("<vaadin-avatar img=\"${item.pictureUrl}\" name=\"${item.fullName}\" alt=\"User avatar\"></vaadin-avatar>")
                    .withProperty("pictureUrl", Animal::getImagenProfile);
        }


    //Métodos funcionales.
    /**
     * Método que recarga la lista que es mostrada en la vista de los animales guardados del usuario activo.
     */
    private void updateListAnimals() {
        grid.setItems(animalfindService.animalFindAllUserSave(authenticatedUser.get().get()));
    }
    /**
     * Método que elimina de la lista que es mostrada en la vista el animal elegido por el usuario activo.
     * @param animal entidad a eliminar.
     */
    private void deleteFromList(Animal animal) {
        if (animal == null)
            return;
        animalfindService.saveDeleteAnimalFromUser(authenticatedUser.get().get(), animal);
        updateListAnimals();
    }

    public Grid.Column<Animal> getDeleteColumn() {
        return deleteColumn;
    }

    public void setDeleteColumn(Grid.Column<Animal> deleteColumn) {
        this.deleteColumn = deleteColumn;
    }
}
