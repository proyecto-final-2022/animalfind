package es.bgonzalez.animalfind.views.pages.user.animals;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.service.AnimalfindService;
import es.bgonzalez.animalfind.security.AuthenticatedUser;
import es.bgonzalez.animalfind.views.MainLayout;

import javax.annotation.security.PermitAll;

/**
 * Clase que crea la vista de los animales del usuario.
 */
@SuppressWarnings("OptionalGetWithoutIsPresent")
@PageTitle("Mis animales")
@Route(value = "my-animals", layout = MainLayout.class)
@RouteAlias(value = "my-animals", layout = MainLayout.class)
@PermitAll
public class MyAnimalsView extends VerticalLayout {
    //Variables.
    private final AuthenticatedUser authenticatedUser;
    private final AnimalfindService animalfindService;
    private AnimalForm animalForm;
    private Grid<Animal> grid;
    private final TextField filterText = new TextField();


    //Constructores.
    /**
     * Constructor por parametros que muestra los animales del usuario activo con los servicios del back-end.
     * @param authenticatedUser usuario activo.
     * @param animalfindService servicios del back-end.
     */
    public MyAnimalsView(AuthenticatedUser authenticatedUser, AnimalfindService animalfindService){
        this.authenticatedUser = authenticatedUser;
        this.animalfindService = animalfindService;

        addClassName("list-view");
        setSizeFull();
            configureGrid();
            configureForm();
        add(getToolbar(), getContent());

        updateList();
        closeEditor();
    }


    //Métodos de la vista.
    /**
     * Método que configura la barra de navegación.
     * @return barra de navegación.
     */
    private HorizontalLayout getToolbar() {
        HorizontalLayout toolbar = new HorizontalLayout();
        toolbar.addClassName("toolbar");
        toolbar.setJustifyContentMode(FlexComponent.JustifyContentMode.BETWEEN);
        toolbar.setAlignItems(FlexComponent.Alignment.CENTER);
        toolbar.setWidthFull();

        filterText.setPlaceholder("Busca por nombre, raza...");
        filterText.setPrefixComponent(VaadinIcon.SEARCH.create());
        filterText.setWidth("50%");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addContactButton = new Button("Añadir animal");
        addContactButton.addClickListener(click -> addAnimal());

        toolbar.add(filterText, addContactButton);
        return toolbar;
    }
    /**
     * Método que configura el cuerpo que contiene el formulario y el grid.
     * @return cuerpo de la vista.
     */
    private Component getContent() {
        HorizontalLayout content = new HorizontalLayout(grid, animalForm);
        content.setFlexGrow(2, grid);
        content.setFlexGrow(1, animalForm);
        content.addClassNames("content");
        content.setSizeFull();
        return content;
    }
    /**
     * Método que configura el grid con sus columnas para mostrar los animales del usuario activo.
     */
    private void configureGrid() {
        grid = new Grid<>(Animal.class, false);

        grid.addColumn(createAvatarRenderer()).setHeader("Image").setAutoWidth(true).setFlexGrow(0).setSortable(true);
        grid.addColumn(Animal::getName).setHeader("Nombre").setSortable(true);
        grid.addColumn(Animal-> Animal.getAnimal_type().getName()).setHeader("Tipo").setSortable(true);
        grid.addColumn(Animal::getBreed).setHeader("Raza").setSortable(true);
        grid.addColumn(Animal::getAge).setHeader("Edad").setSortable(true);
        grid.addColumn(Animal::getStatus).setHeader("Disponibilidad").setSortable(true);
        grid.addColumn(Animal::getDescription).setHeader("Description").setSortable(true);

        grid.asSingleSelect().addValueChangeListener(event ->
                editAnimal(event.getValue()));
    }
        /**
         * Sub-método para poder mostrar la imagen del animal.
         * @return imagen.
         */
        private static Renderer<Animal> createAvatarRenderer() {
            return LitRenderer.<Animal>of(
                            "<vaadin-avatar img=\"${item.pictureUrl}\" name=\"${item.fullName}\" alt=\"User avatar\"></vaadin-avatar>")
                    .withProperty("pictureUrl", Animal::getImagenProfile);
        }
    /**
     * Método que configura el formulario.
     */
    private void configureForm() {
        animalForm = new AnimalForm(animalfindService.AnimalTypesFindAll());
        animalForm.setWidth("25em");

        animalForm.addListener(AnimalForm.SaveEvent.class, this::saveAnimal);
        animalForm.addListener(AnimalForm.DeleteEvent.class, this::deleteAnimal);
        animalForm.addListener(AnimalForm.CloseEvent.class, e -> closeEditor());
    }
    /**
     * Método que cierra el editor de animales.
     */


    //Métodos de funcionalidad.
    private void closeEditor() {
        animalForm.setAnimal(null);
        animalForm.setVisible(false);
        removeClassName("editing");
    }
    /**
     * Método que edita a un animal existente de la lista del usuario activo cuando detecta el evento.
     * @param animal a actualizar.
     */
    public void editAnimal(Animal animal) {
        if (animal == null) {
            closeEditor();
        } else {
            animalForm.setAnimal(animal);
            animalForm.setVisible(true);
            addClassName("editing");
        }
    }
    /**
     * Método que abre la posibilidad de añadir a un animal a la lista del usuario activo cuando detecta el evento.
     */
    private void addAnimal() {
        grid.asSingleSelect().clear();
        editAnimal(new Animal());
    }
    /**
     * Método que actualiza a un animal de la lista del usuario activo cuando detecta el evento.
     * @param event evento.
     */
    private void saveAnimal(AnimalForm.SaveEvent event) {
        event.getAnimal().setUser(authenticatedUser.get().get());
        animalfindService.animalSave(event.getAnimal());
        updateList();
        closeEditor();
    }
    /**
     * Método que elimina a un animal de la lista del usuario activo cuando detecta el evento.
     * @param event evento.
     */
    private void deleteAnimal(AnimalForm.DeleteEvent event) {
        animalfindService.animalDelete(event.getAnimal());
        updateList();
        closeEditor();
    }
    /**
     * Método que recarga la lista que es mostrada del usuario activo.
     */
    private void updateList() {
        grid.setItems(animalfindService.animalFindAllUser(filterText.getValue(), authenticatedUser.get().get()));
    }

}
