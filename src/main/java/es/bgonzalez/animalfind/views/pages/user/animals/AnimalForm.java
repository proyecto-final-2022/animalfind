package es.bgonzalez.animalfind.views.pages.user.animals;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;
import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.entity.AnimalType;

import java.util.List;

/**
 * Clase que crea el formulario de edición del usuario.
 */
public class AnimalForm extends FormLayout {
  //Variables.
  private final Binder<Animal> binder = new BeanValidationBinder<>(Animal.class);
  private Animal animal;
  private final TextField name = new TextField("Nombre");
  private final ComboBox<AnimalType> animalType = new ComboBox<>("Tipo");
  private final ComboBox<String> status = new ComboBox<>("Estado");
  private final TextField breed = new TextField("Raza");
  private final IntegerField age = new IntegerField("Edad");
  private final TextArea description = new TextArea ("Description");
  private final TextField imagenProfile = new TextField("URL de imagen");
  private final Button save = new Button("Guardar");
  private final Button delete = new Button("Eliminar");
  private final Button close = new Button("Cancelar");


  //Constructores
  /**
   * Constructor por defecto que pinta el formulario.
   * @param animalTypes lista de los tipos de animales.
   */
  public AnimalForm(List<AnimalType> animalTypes) {
    addClassName("contact-form");

    //Campos del formulario.
    animalType.setItems(animalTypes);
    animalType.setItemLabelGenerator(AnimalType::getName);
    status.setItems("Disponible", "No disponible", "En reserva");

    //Asignación de entidad bean.
    binder.bindInstanceFields(this);

    //Añadido las campo al formulario.
    setColspan(description, 2);
    add(name, animalType, breed, age, status, imagenProfile, description, createButtonsLayout());
  }


  //Métodos funcionales.
  /**
   * Método que asigna a un animal al formulario.
   * @param animal animal a a asignar.
   */
  public void setAnimal(Animal animal) {
    this.animal = animal;
    binder.readBean(animal);
  }
  /**
   * Método que añade a un animal nuevo si es necesario.
   * @param eventType evento.
   *            the component event type, not <code>null</code>
   * @param listener escucha
   *            the listener to add, not <code>null</code>
   * @return el estado de registración.
   * @param <T> t.
   */
  public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
    return getEventBus().addListener(eventType, listener);
  }
  /**
   * Método que crea los botones.
   * @return botones.
   */
  private Component createButtonsLayout() {
    save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    delete.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
    close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

    save.addClickShortcut(Key.ENTER);
    close.addClickShortcut(Key.ESCAPE);

    save.addClickListener(event -> validateAndSave());
    delete.addClickListener(event -> fireEvent(new DeleteEvent(this, animal)));
    close.addClickListener(event -> fireEvent(new CloseEvent(this)));

    binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
    return new HorizontalLayout(save, delete, close);
  }
  /**
   * Método que valida y guarda a un animal.
   */
  private void validateAndSave() {
    try {
      binder.writeBean(animal);
      fireEvent(new SaveEvent(this, animal));
    } catch (ValidationException e) {
      e.printStackTrace();
    }
  }


  //Clases.
  /**
   * Clase para controlar los eventos.
   */
  public static abstract class ContactFormEvent extends ComponentEvent<AnimalForm> {
    private final Animal animal;
    protected ContactFormEvent(AnimalForm source, Animal animal) {
      super(source, false);
      this.animal = animal;
    }
    public Animal getAnimal() {
      return animal;
    }
  }
  /**
   * Clase que guarda el evento.
   */
  public static class SaveEvent extends ContactFormEvent {
    SaveEvent(AnimalForm source, Animal animal) {
      super(source, animal);
    }
  }
  /**
   * Clase que elimina el evento.
   */
  public static class DeleteEvent extends ContactFormEvent {
    DeleteEvent(AnimalForm source, Animal animal) {
      super(source, animal);
    }

  }
  /**
   * Clase que cierra el evento.
   */
  public static class CloseEvent extends ContactFormEvent {
    CloseEvent(AnimalForm source) {
      super(source, null);
    }
  }

  public Button getDelete() {
    return delete;
  }

  public Button getSave() {
    return save;
  }

  public TextField getName() {
    return name;
  }

  public ComboBox<AnimalType> getAnimalType() {
    return animalType;
  }

  public ComboBox<String> getStatus() {
    return status;
  }

  public TextField getBreed() {
    return breed;
  }

  public IntegerField getAge() {
    return age;
  }

  public TextArea getDescription() {
    return description;
  }

  public TextField getImagenProfile() {
    return imagenProfile;
  }
}