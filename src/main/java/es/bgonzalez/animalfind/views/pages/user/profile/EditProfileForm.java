package es.bgonzalez.animalfind.views.pages.user.profile;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;

/**
 * Clase que crea la vista del formulario de edición.
 */
public class EditProfileForm extends FormLayout {
    //Variables.
    private final TextField username;
    private final EmailField email;
    private final PasswordField password;
    private final PasswordField confirmPassword;
    private final PhoneNumberField phone;
    private final TextField profilePictureUrl;
    private final Image image;
    private final Button savesButton;
    private final Button restartButton;
    private final Span errorMessageField;


    //Constructores.
    /**
     * Constructor por defecto que realiza la forma del formulario de edición a un usuario.
     */
    public EditProfileForm(){
        //Cabecera
        VerticalLayout headerContainer = new VerticalLayout();
            H2 header = new H2("Editar Perfil");
            header.addClassNames("mb-0", "mt-xl", "text-3xl");
            Paragraph description = new Paragraph("Edite su perfil de AnimalFind!");
            description.addClassNames("mt-0", "text-secondary");
            image = new Image();
            image.setWidth("100px");
            image.setHeight("100px");
            image.setSrc("empty");
        headerContainer.add(header, description, image);

        //Cuerpo
        username = new TextField("Nombre de usuario");
        email = new EmailField("Correo electronico");
        password = new PasswordField("Contraseña");
        confirmPassword = new PasswordField("Confirmar contraseña");
        phone = new PhoneNumberField("Numero de teléfono");
        profilePictureUrl = new TextField("Imagen de perfil");
        errorMessageField = new Span();

        //Footer
        savesButton = new Button("Guardar campos");
        savesButton.addThemeVariants(ButtonVariant.LUMO_SUCCESS);
        restartButton = new Button("Restablecer campos");
        restartButton.addThemeVariants(ButtonVariant.LUMO_ERROR);


        //Atributos
        add(headerContainer, username, email, password, confirmPassword, phone, profilePictureUrl, errorMessageField, savesButton, restartButton);
        setMaxWidth("800px");
        setColspan(headerContainer, 2);
        setColspan(phone, 2);
        setColspan(profilePictureUrl, 2);
        setColspan(savesButton, 2);
        setColspan(restartButton, 2);

    }


    //Getters and setters.
    /**
     * Método que obtiene una imagen del usuario.
     * @return imagen del usuario.
     */
    public Image getImage() {
        return image;
    }
    /**
     * Método que obtiene el field de nombre del usuario.
     * @return nombre del usuario.
     */
    public TextField getUsername() {
        return username;
    }
    /**
     * Método que obtiene el field de email del usuario.
     * @return email del usuario.
     */
    public EmailField getEmail() {
        return email;
    }
    /**
     * Método que obtiene el field de teléfono del usuario.
     * @return teléfono del usuario.
     */
    public PhoneNumberField getPhone() {
        return phone;
    }
    /**
     * Método que obtiene el field de url del usuario.
     * @return url.
     */
    public TextField getProfilePictureUrl() {
        return profilePictureUrl;
    }
    /**
     * Método que obtiene el field de contraseña.
     * @return contraseña.
     */
    public PasswordField getPassword() {
        return password;
    }
    /**
     * Método que obtiene el field de contraseña confirmada.
     * @return contraseña confirmada.
     */
    public PasswordField getConfirmPassword() {
        return confirmPassword;
    }
    /**
     * Método que obtiene el botón de registrarse.
     * @return botón de registrarse.
     */
    public Button getSavesButton() {
        return savesButton;
    }
    /**
     * Método que obtiene el botón de restablecer.
     * @return botón de restablecer.
     */
    public Button getRestartButton() {
        return restartButton;
    }
    /**
     * Método que obtiene el error.
     * @return mensaje de error.
     */
    public Span getErrorMessageField() {
        return errorMessageField;
    }


    //Clase.
    /**
     * Realizar una field personalizada.
     */
    public static class PhoneNumberField extends CustomField<String> {
        private final ComboBox<String> countryCode = new ComboBox<>();
        private final TextField number = new TextField();

        public PhoneNumberField(String label) {
            setLabel(label);
            countryCode.setWidth("120px");
            countryCode.setPlaceholder("Country");
            countryCode.setPattern("\\+\\d*");
            countryCode.setPreventInvalidInput(true);
            countryCode.setItems("+33", "+34");
            countryCode.addCustomValueSetListener(e -> countryCode.setValue(e.getDetail()));
            number.setPattern("\\d*");
            number.setPreventInvalidInput(true);
            HorizontalLayout layout = new HorizontalLayout(countryCode, number);
            layout.setFlexGrow(1.0, number);
            add(layout);
        }

        @Override
        protected String generateModelValue() {
            if (countryCode.getValue() != null && number.getValue() != null) {
                return countryCode.getValue() + " " + number.getValue();
            }
            return "";
        }

        @Override
        protected void setPresentationValue(String phoneNumber) {
            String[] parts = phoneNumber != null ? phoneNumber.split(" ", 2) : new String[0];
            if (parts.length == 1) {
                countryCode.clear();
                number.setValue(parts[0]);
            } else if (parts.length == 2) {
                countryCode.setValue(parts[0]);
                number.setValue(parts[1]);
            } else {
                countryCode.clear();
                number.clear();
            }
        }
    }
}
