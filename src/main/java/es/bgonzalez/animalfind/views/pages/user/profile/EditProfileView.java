package es.bgonzalez.animalfind.views.pages.user.profile;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.*;
import es.bgonzalez.animalfind.data.service.AnimalfindService;
import es.bgonzalez.animalfind.security.AuthenticatedUser;
import es.bgonzalez.animalfind.views.MainLayout;
import es.bgonzalez.animalfind.views.pages.home.HomeView;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.security.PermitAll;

/**
 * Clase que pinta la página de edición de usuario.
 */
@PageTitle("Editar Perfil")
@Route(value = "edit-profile", layout = MainLayout.class)
@RouteAlias(value = "edit-profile", layout = MainLayout.class)
@PermitAll
public class EditProfileView extends VerticalLayout implements BeforeEnterObserver {
    //Atributos
    private final AuthenticatedUser authenticatedUser;


    //Constructores.
    /**
     * Constructor por parametros para poder realizar el alta de un usuario nuevo.
     * @param authenticatedUser usuario activo.
     * @param service servicio de back-end.
     * @param passwordEncoder encriptado.
     */
    public EditProfileView(AuthenticatedUser authenticatedUser, AnimalfindService service, PasswordEncoder passwordEncoder){
        this.authenticatedUser =  authenticatedUser;
        EditProfileForm editProfileForm = new EditProfileForm();
        setHorizontalComponentAlignment(Alignment.CENTER, editProfileForm);
        add(editProfileForm);
        EditProfileFormBinder editProfileFormBinder = new EditProfileFormBinder(editProfileForm);
        editProfileFormBinder.addBindingAndValidation(authenticatedUser, service, passwordEncoder);
    }


    //Métodos.
    /**
     * Método para validar que el usuario es activo.
     * @param event comprobación antes de entrar.
     */
    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if(authenticatedUser.get().isEmpty()){
            Notification.show("No puede realizar esta acción. Ya esta registrado.").addThemeVariants(NotificationVariant.LUMO_ERROR);
            event.forwardTo(HomeView.class);
        }
    }
}
