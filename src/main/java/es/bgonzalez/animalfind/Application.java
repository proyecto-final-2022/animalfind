package es.bgonzalez.animalfind;

import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import org.postgresql.util.PSQLException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.net.ConnectException;

/**
 * El punto de entrada de Spring Boot application.
 *
 * Us la @PWA anotacion para hacer instalables es moviles, tablets
 * y algunos navegadores..
 *
 */
@SpringBootApplication
@NpmPackage(value = "@fontsource/open-sans", version = "4.5.0")
@Theme(value = "animalfind" )
@PWA(
        name = "AnimalFind",
        shortName = "animalfind",
        offlinePath="offline.html",
        offlineResources = { "./images/offline.webp"}
)
@NpmPackage(value = "line-awesome", version = "1.3.0")
public class Application extends SpringBootServletInitializer implements AppShellConfigurator {

    public static void main(String[] args) {
            SpringApplication.run(Application.class, args);
    }

}
