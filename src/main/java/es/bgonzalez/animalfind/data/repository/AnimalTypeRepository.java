package es.bgonzalez.animalfind.data.repository;

import es.bgonzalez.animalfind.data.entity.AnimalType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnimalTypeRepository extends JpaRepository<AnimalType, Integer> {
    AnimalType findByName(String name);
}