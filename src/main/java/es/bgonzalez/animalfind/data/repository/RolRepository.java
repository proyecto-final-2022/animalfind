package es.bgonzalez.animalfind.data.repository;

import es.bgonzalez.animalfind.data.entity.Rol;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RolRepository extends JpaRepository<Rol, Integer> {
    Rol findByName(String name);

}