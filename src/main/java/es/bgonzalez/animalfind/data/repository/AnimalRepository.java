package es.bgonzalez.animalfind.data.repository;

import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.entity.AnimalType;
import es.bgonzalez.animalfind.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AnimalRepository extends JpaRepository<Animal, Integer> {

    @Query("select a from Animal a " +
            "where lower(a.name) like lower(concat('%', :searchTerm, '%')) " +
            "or lower(a.breed) like lower(concat('%', :searchTerm, '%')) " +
            "or lower(a.user.username) like lower(concat('%', :searchTerm, '%')) ")
    List<Animal> search(@Param("searchTerm") String searchTerm);
    @Query("select a from Animal a " +
            "where a.animal_type = :animalType " +
        "and (lower(a.name) like lower(concat('%', :searchTerm, '%')) " +
        "or lower(a.breed) like lower(concat('%', :searchTerm, '%'))" +
        "or lower(a.user.username) like lower(concat('%', :searchTerm, '%')) )")
        List<Animal> searchWithFilters(@Param("searchTerm") String searchTerm, @Param("animalType") AnimalType animalType);
@Query("select a from Animal a " +
        "where a.animal_type = :animalType ")
    List<Animal> searchOnlyFilters(@Param("animalType") AnimalType animalType);
        List<Animal> findByUser(User user);
@Query("select a from Animal a " +
        "where a.user = :user " +
        "and (lower(a.name) like lower(concat('%', :searchTerm, '%')) " +
        "or lower(a.breed) like lower(concat('%', :searchTerm, '%'))) " )
    List<Animal> searchUser(@Param("searchTerm") String searchTerm,@Param("user") User user);
@Query("select a from Animal a inner join Save s " +
        "on a = s.animal " +
        "where s.user = :user ")
    List<Animal> searchUserSave(@Param("user") User user);
        }