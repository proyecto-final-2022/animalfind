package es.bgonzalez.animalfind.data.repository;

import es.bgonzalez.animalfind.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);
}