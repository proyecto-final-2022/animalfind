package es.bgonzalez.animalfind.data.repository;

import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.entity.Booking;
import es.bgonzalez.animalfind.data.entity.Save;
import es.bgonzalez.animalfind.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Integer> {

    List<Booking> findByUser(User user);
    @Query("select b from Booking b " +
            "where b.user = :user " +
            "and b.animal = :animal ")
    Booking searchFromUserList(@Param("user") User user, @Param("animal") Animal animal);

    @Modifying
    @Query("delete from Booking " +
            "where user = :user " +
            "and animal = :animal ")
    void deleteFromUserList(@Param("user") User user, @Param("animal") Animal animal);
}