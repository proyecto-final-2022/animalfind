package es.bgonzalez.animalfind.data.repository;

import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.entity.Save;
import es.bgonzalez.animalfind.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SaveRepository extends JpaRepository<Save, Integer> {
    List<Save> findByUser(User user);
    @Query("select s from Save s " +
            "where s.user = :user " +
            "and s.animal = :animal ")
    Save searchFromUserList(@Param("user") User user, @Param("animal") Animal animal);
    @Modifying
    @Query("delete from Save " +
            "where user = :user " +
            "and animal = :animal ")
    void deleteFromUserList(@Param("user") User user, @Param("animal") Animal animal);
}