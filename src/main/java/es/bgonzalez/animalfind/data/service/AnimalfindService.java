package es.bgonzalez.animalfind.data.service;

import es.bgonzalez.animalfind.data.entity.*;
import es.bgonzalez.animalfind.data.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AnimalfindService{
    //Variables.
    private final RolRepository rolRepository;
    private final UserRepository userRepository;
    private final AnimalTypeRepository animalTypeRepository;
    private final AnimalRepository animalRepository;
    private final SaveRepository saveRepository;
    private final BookingRepository bookingRepository;

    /**
     * Constructor que une los repositorios de las entidades en esta clase servicio para que sea más fácil de manipular.
     * @param rolRepository
     * @param userRepository
     * @param animalTypeRepository
     * @param animalRepository
     * @param saveRepository
     */
    @Autowired
    public AnimalfindService(RolRepository rolRepository, UserRepository userRepository, AnimalTypeRepository animalTypeRepository, AnimalRepository animalRepository, SaveRepository saveRepository, BookingRepository bookingRepository) {
        this.rolRepository = rolRepository;
        this.userRepository = userRepository;
        this.animalTypeRepository = animalTypeRepository;
        this.animalRepository = animalRepository;
        this.saveRepository = saveRepository;
        this.bookingRepository = bookingRepository;
    }

    //RolRepository
    public Rol RolFindByName(String user) {return rolRepository.findByName(user);}

    //UserRepository
    public void userUpdate(User entity) {
        userRepository.save(entity);
    }
    public void userCreate(User user){
        userRepository.saveAndFlush(user);
    }

    //AnimalTypeRepository
    public List<AnimalType> AnimalTypesFindAll() {
        return animalTypeRepository.findAll();
    }

    //AnimalRepository
    public List<Animal> animalFindAll(String stringFilter, AnimalType animalType) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            if(animalType == null){
                return animalRepository.findAll();
            } else{
                return animalRepository.searchOnlyFilters(animalType);
            }
        } else if(animalType == null){
            return animalRepository.search(stringFilter);
        }else {
            return animalRepository.searchWithFilters(stringFilter, animalType);
        }
    }
    public List<Animal> animalFindAllUserSave(User user){return animalRepository.searchUserSave(user);}
    public List<Animal> animalFindAllUser(String stringFilter, User user) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return animalRepository.findByUser(user);
        } else {
            return animalRepository.searchUser(stringFilter, user);
        }
    }

    public List<Animal> animalFindByUser(User user) {
            return animalRepository.findByUser(user);
    }
    public void animalDelete(Animal animal) {
        animalRepository.delete(animal);
    }
    public void animalSave(Animal animal) {
        animalRepository.save(animal);
    }

    //Save
    public void saveCreate(Save save){saveRepository.saveAndFlush(save);}
    @Transactional
    public void saveDeleteAnimalFromUser(User user, Animal animal){saveRepository.deleteFromUserList(user, animal);}
    public Save saveSearchSaveFromUser(User user, Animal animal){return saveRepository.searchFromUserList(user, animal);}

    //Save
    public void bookingCreate(Booking booking){bookingRepository.saveAndFlush(booking);}
    @Transactional
    public void bookingDeleteAnimalFromUser(User user, Animal animal){bookingRepository.deleteFromUserList(user, animal);}
    public Booking bookingSearchSaveFromUser(User user, Animal animal){return bookingRepository.searchFromUserList(user, animal);}
}
