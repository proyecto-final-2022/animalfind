package es.bgonzalez.animalfind.data.generator;

import com.vaadin.flow.spring.annotation.SpringComponent;
import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.entity.AnimalType;
import es.bgonzalez.animalfind.data.entity.Rol;
import es.bgonzalez.animalfind.data.entity.User;
import es.bgonzalez.animalfind.data.repository.AnimalRepository;
import es.bgonzalez.animalfind.data.repository.AnimalTypeRepository;
import es.bgonzalez.animalfind.data.repository.RolRepository;
import es.bgonzalez.animalfind.data.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringComponent
public class DataGenerator {

    @Bean
    public CommandLineRunner loadData(PasswordEncoder passwordEncoder, UserRepository userRepository, RolRepository rolRepository, AnimalTypeRepository animalTypeRepository, AnimalRepository animalRepository) {
        return args -> {
            Logger logger = LoggerFactory.getLogger(getClass());
            if (userRepository.count() != 0L) {
                logger.info("Using existing database");
                return;
            }
            int seed = 123;

            logger.info("Generating demo data");

                /**
                 * Generador de roles.
                 */
                logger.info("... generating 2 Rols entities...");
                generadorRoles(rolRepository);

                /**
                 * Generador de usuarios.
                 */
                logger.info("... generating 2 Users entities...");
                generadorUsuarios(passwordEncoder, userRepository, rolRepository);

                /**
                 * Generador de tipos de animales.
                 */
                logger.info("... generating 7 AnimalTypes entities...");
                generarTiposAnimales(animalTypeRepository);

                /**
                 * Generador de animales.
                 */
                logger.info("... generating 14 Animals entities...");
                generarAnimales(userRepository, animalTypeRepository, animalRepository);

            logger.info("Generated demo data");

        };
    }

    private void generadorRoles(RolRepository rolRepository) {
        Rol rolUser = new Rol();
        rolUser.setName("USER");
        rolRepository.save(rolUser);
        Rol rolAdmin = new Rol();
        rolAdmin.setName("ADMIN");
        rolRepository.save(rolAdmin);
    }

    private void generadorUsuarios(PasswordEncoder passwordEncoder, UserRepository userRepository, RolRepository rolRepository) {
        User user = new User();
        user.setUsername("John Normal");
        user.setEmail("user@animalfind.es");
        user.setHashedPassword(passwordEncoder.encode("User-123"));
        user.setPhone("+34 600000001");
        user.setProfilePictureUrl(
                "https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=128&h=128&q=80");
        user.setRol(rolRepository.findByName("USER"));
        userRepository.save(user);

        User admin = new User();
        admin.setUsername("Emma Powerful");
        admin.setEmail("admin@animalfind.es");
        admin.setHashedPassword(passwordEncoder.encode("Admin-123"));
        admin.setPhone("+34 600000002");
        admin.setProfilePictureUrl(
                "https://images.unsplash.com/photo-1607746882042-944635dfe10e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=128&h=128&q=80");
        admin.setRol(rolRepository.findByName("ADMIN"));
        userRepository.save(admin);

        User user1 = new User();
        user1.setUsername("Bryan Gonzalez");
        user1.setEmail("bryan@animalfind.es");
        user1.setHashedPassword(passwordEncoder.encode("Bryan-123"));
        user1.setPhone("+34 600000003");
        user1.setProfilePictureUrl(
                "https://images.unsplash.com/photo-1607746882042-944635dfe10e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=128&h=128&q=80");
        user1.setRol(rolRepository.findByName("USER"));
        userRepository.save(user1);
    }

    private void generarTiposAnimales(AnimalTypeRepository animalTypeRepository) {
        AnimalType type1 = new AnimalType();
        type1.setName("Perros");
        animalTypeRepository.save(type1);

        AnimalType type2 = new AnimalType();
        type2.setName("Gatos");
        animalTypeRepository.save(type2);

        AnimalType type3 = new AnimalType();
        type3.setName("Aves");
        animalTypeRepository.save(type3);

        AnimalType type4 = new AnimalType();
        type4.setName("Peces");
        animalTypeRepository.save(type4);

        AnimalType type5 = new AnimalType();
        type5.setName("Roedores");
        animalTypeRepository.save(type5);

        AnimalType type6 = new AnimalType();
        type6.setName("Insectos");
        animalTypeRepository.save(type6);

        AnimalType type7 = new AnimalType();
        type7.setName("Otros");
        animalTypeRepository.save(type7);
    }

    private void generarAnimales(UserRepository userRepository, AnimalTypeRepository animalTypeRepository, AnimalRepository animalRepository) {
        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Labrador",
                "https://upload.wikimedia.org/wikipedia/commons/0/04/Labrador_Retriever_%281210559%29.jpg",
                "Gato",
                "Perros",
                "bryan@animalfind.es",
                "Disponible",
                "El labrador retriever es una raza canina originaria de Terranova, en la actual Canadá. Es una de las razas más populares del mundo por la cantidad de ejemplares registrados."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Rottweiler",
                "https://upload.wikimedia.org/wikipedia/commons/f/fb/02_I_Exposici%C3%B3n_Monogr%C3%A1fica_Club_Rottweiler_de_Espa%C3%B1a_-_Santa_Brigida_-_Gran_Canaria.jpg",
                "Mikasa",
                "Perros",
                "user@animalfind.es",
                "Disponible",
                "El rottweiler es una raza canina de tipo molosoide originaria de Alemania, aunque fue también usado en la Antigua Roma."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Siames",
                "https://www.zooplus.es/magazine/wp-content/uploads/2019/01/Gato-siames.jpg",
                "Maria",
                "Gatos",
                "admin@animalfind.es",
                "Disponible",
                "El siamés es una raza de gato. Dentro de dicha raza se distinguen dos variedades: por un lado el siamés moderno, y por otro el siamés tradicional o Thai."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Sphynx",
                "https://www.disane.es/wp-content/uploads/gato_egipcio.jpg",
                "Panela",
                "Gatos",
                "bryan@animalfind.es",
                "Disponible",
                "El Sphynx o gato esfinge es una raza de gato. Pertenece a la familia de los félidos y al género Felis. La característica más llamativa de esta raza es la aparente ausencia de pelaje y su aspecto delgado y esbelto."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Canario",
                "https://www.tiendanimal.es/articulos/wp-content/uploads/2010/05/pig-1200x900.jpg",
                "Pita",
                "Aves",
                "user@animalfind.es",
                "Disponible",
                "El canario silvestre o serín canario \u200B es una especie de ave paseriforme de la familia de los fringílidos. A pesar de su nombre tanto común como científico es autóctono de varios subarchipiélagos de la Macaronesia: las islas Canarias, Azores y Madeira, y no solo de las primeras."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Loro",
                "https://estaticos-cdn.elperiodico.com/clip/9a36bb77-0c88-4b3c-a7dc-f3d41dd85987_alta-libre-aspect-ratio_default_0.jpg",
                "Simpia",
                "Aves",
                "admin@animalfind.es",
                "Disponible",
                "Los psitacoideos son una superfamilia del orden Psittaciformes, que incluye los loros típicos, un total de 369 especies."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Amphiprioninae",
                "https://i.ytimg.com/vi/Ywqu3xGd6AE/maxresdefault.jpg",
                "Payaso",
                "Peces",
                "bryan@animalfind.es",
                "Disponible",
                "Amphiprioninae es una subfamilia de peces marinos de la familia Pomacentridae, que engloba únicamente a los géneros Amphiprion y Premnas, cuyos componentes son conocidos como peces payaso o peces de las anémonas."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Desconocida",
                "https://i.ytimg.com/vi/dZfV-8BlLhQ/maxresdefault.jpg",
                "Musolini",
                "Peces",
                "user@animalfind.es",
                "Disponible",
                "Mussolini es el pez malcriado y comilón del youtuber Dross. Su nombre esta inspirado en el dictador italiano Benito Mussolini."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Tarantula",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Brachypelma_smithi_2009_G03.jpg/1200px-Brachypelma_smithi_2009_G03.jpg",
                "Alvaro",
                "Insectos",
                "admin@animalfind.es",
                "Disponible",
                "Se llama tarántulas a las arañas más grandes de la familia licósidos, especialmente miembros del género Lycosa como Lycosa tarantula. Los colonizadores del Nuevo Mundo pasaron a llamar \"tarántulas\" a las arañas, mucho más grandes, de las familias americanas Theraphosidae y Dipluridae."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Gusano",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Nerr0328.jpg/1200px-Nerr0328.jpg",
                "Unai",
                "Insectos",
                "bryan@animalfind.es",
                "Disponible",
                "La palabra gusano se utiliza coloquialmente para designar los más diversos animales que coinciden en ser pequeños, blandos, de forma alargada y con apéndices locomotores poco destacados o ausentes. Algunos o todos los miembros de los siguientes grupos coinciden con la idea que popularmente se tiene de los gusanos:"
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Cricetinae",
                "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRUVFhYYFRUaGBgYFRUYFRIYGBgYGBgZGRgYGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHhISHDQhISExNDExNDExNDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0MTExNDQ0Pz8xMT8/PzQ0MTExMf/AABEIAOEA4AMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAEBQIDBgEABwj/xAA3EAABAwMDAgQFAwMDBQEAAAABAAIRAwQhBRIxQVEiYXGBBhMykbEUodFCUvDB4fEzNGJywiP/xAAaAQADAQEBAQAAAAAAAAAAAAAAAQIDBAUG/8QAIREAAgICAgMBAQEAAAAAAAAAAAECERIhAzEEQVEiMhP/2gAMAwEAAhEDEQA/AAadVW/MUH6Y9hiJXBSd2XnSiespok6soOrqxtm93ARFDRKjukKcWPOItfUVRkrRN+HHdVfS0CFSizOXLEzNO1ceiJbbbei1DdLgR/oujSx1UT4nL2EeaKMrtPZdbavdw0n2WtpaawHhM6DGN4aFMfHXtjl5fxGGZoFZ/wDTCJZ8I1DyYW/ZcN7BWC6b2W8eOKMZeRJmBo/CJDvEcITVNMZTwMr6M+6bnAWL+JK7c4VpJPRlKcn2z51qpO6BhP8A4bp7Wgkys5q9SX4Wj+HWEtE8LSVUSnZstKZvdud9ISz46vW7Q1uSmFrVOGNSH4os9jg4mSf2Wa7AwlzJcZ5UQwDJRN24Bzj1lBPdK1QHHulRDFaxii+oBgcpgecA0SeeyhTBeROG9lCJ5RNpyiTpDj2dvmBrIAhUWI8Ku1LhU2xhmcJJNxLbSkXlQKpq3Y4bkqoUnv5MBCj9E230fT2as97z4T9kW17zks/ZCafIetE2pjhZPQhfS1HaYLP2Ta3viRgJTev6wjdOu8cJMYybdO7Louj2UBdDsom9YOY/ZKxUXi58l43HkhHanSHJH3CpfrFDuPuiwxYyFcdlays3skj9bo9x90BW+IGSQPujIpQZrhXYouuqfksBW+IXGIHCX19Ve6SMIyKXEz6TWvKQGSPusbr91Td9J/dZyrdvdyT90I5jj1KaY/8AEB1Nw3Y7rTaG87AB2WfNueyaafdmmIIlU5Jon/Jro3elFrBuPKynxlqO94DOiY6bfNeQCY8kq+IGs3+HvkpReyHF2YasXFx7yrqTIy77I+qxoJxlDvZK1yFQHcXBOBwh9xTA0AvfpQmmJoXh5RtmYycBQqOY3gSVGi0vOeOyHVbHFO9E7y9bwBKrpUXPyTjsuXlMAYRVs3whJuo6Go3LZ1lBreAuuCmQoOCjbNOj6FR1ENdJH7JmzXmRwPssm96EfcQsE2aOCZta2osf0CKtL6kxuYC+cv1Ajgprotu981HyWj6ZPJ9FW6BcVmru9XYcMbjvwklxT3kkvcJ7FdrNKFdIU2dEOGIJd6O/ljy7yJSt1q9ph0rQMqOHVFUix/hePRPIqXFXQjtrWeUSLVFXlk6mZb4mfuF61qbjB68KWZtUDi1UjbYTE0oK78tAhV+nUf089E2FPKm+kBBRYCF9vCrfTTirTlA1KJlMkBbIMjC9UG7JOVa+nCpVJiaspfaql9rCPY9Tc0EKlJkOKEb3gcZKp+W9/f0CbM07c7CvudtIAN57q8vhngIX2Dv7Sr6NoWhXu1AqP6zdM4SlkyopIV6gOEXbfSELd1GTkyuCq92GNx3WuOkZX+nQY94CHfctHVQ/RvP1O9layxYOclL8of6Y5uLqEA+rK87KgAskkjqPNElfRdPobaTGj+0LAUGZB819Ps2yxh7tH4UyZS0BmhPRUXFpKaPMKvcCoLjJiJ1q4dFWAQZ7LVUabTyqL+1YWmBnuk0aLkfQpdeAgSuU2NwQIlB1bWCAcz2PCb2VqIABJb1nkISJnJUXfIlQdbnsjWshVuPfCGYWBGmuP4VtWtEgIV1XugDopk9FF1p3ELvzCR4TBXGXRBhyYFFazBwOUrurQtPCbVKviEI97GPbBGe6BPRlG0ldQZmCmNWwLZgIdtIgpkkjbRkJDqtJ0yeFonmRHEJbcvlpBCuLEZz5JVbm9E6YwQld0yHLRMgU3LBvEJ1SwB6JPWy8eoTdic+kTDtkwJUHq9w2iOpQzlCRdk4UmU8qbGIhjEmzoojTYtV8P6n4flnoPD6dlnWsV1JhBBGCoYzXXNbCXi4kqplVzhBPRDta5ru4Uloe0axDUJfXD4JC7RriBKJphrpb3CBp0J9KudzyOT13OgegC01F4xIDSP7UlttO2O3DlNrYHqgz5HbLq74OULUMomvTBVJpHpBSIAn0QTyo1bTwnafFGCjAw9lytbk5b+UWPs+eXNO9pvyXuE4Le3pHKJp66Q8tfuAGAXgbvR0YWzfPDh9wqalqx3LGn1aFrnF9ozxaemLLS6Y8iDlOWYVNHSKMzsDTz4ZH4TH5IjCh16Lbsoc8cFCXNEASpXYIKEZUMEHISsmilwBQdyzEBFtElTq23uqTJZnwwtMFC37BEpzqNvgHsk965pZt6laRZDEjw3dulFUr5oM8gIBtMb46SnTbdkcBaSaREU2L6uouJmFUbt39qbfLaOAFAoyXweL+hrGFFU6a5RajWUysGzsK2U4U2Nyrfl9lOjTO7hQNBDqfhBC7TIKY0beWpZf2j2Hc0FBaaCmWs8K5lk4GZ/dKKWoVm42yemE1sn1XjxtA9DBSsHoZCR091P53svUwG8T75UnCen7YQzFlT6w6yfRda8efrlRqCOo9iuMjzlAUFAk4mfUD8rwEdx6RCjTqR/cfYELz63k5IVHHsB81UKCmKpPl6wrQ/vH4QBVA4UDUAwSo13YxM90EZPKAo7dCUAGo9gJVFakd3CYUUsaER0VL2EZUXvhqaJkgW9e0giVm9QowZHCb1ckkod72cFaxM2ZWg3/9E3lUVbfa/HEypPqAdVo/0THRNzlFca6V0lTix2aCiYRQeUOwK4LBnUju4qdDJyqirKPKRRobGmNoglEVbbcIQmnvIjqmTagKZLEv6Atf3CYst++feIRJIXCZSBuzxpdvyFDYex+7VLYF0M/ySkKgd9I9RlcbQ+6JLfNQc0jv+yYHWMI5P2Xt47n3UDVf6jtMKl9y0mPl57kykFEqrB0cSfKELVe8easqExgR6KkbvM+RCYE6VfoRCk6nPCqcPLafOUVRqiI/3QBSKcZ6rrs5V78rrqRDcBAAz6O4Qll7RIwm9KZk8KVy1rh5pksxVZ8SEvrnqn97ZkEpPcW60i0ZSRO0oNewknI4SWrpz9xzCb6dW2PyJBRGq0Tu3AYOVeTTJxTM1UpPHHCrPzB0lONoHOVW9/ZVmTgjTvpwVGEZUYqS0rmO5A+0q6kxeDO6KosHqkMYWQ4TWkEqt2z5JxbUxHdBEjrmKg46op+EPVHVAkVvYDndCi6oBjleL1U9/YJF0cdVH+EqTah7E+coWo4D/lVPuT2P3THiFuqFTbdRiJ8gClzLgzAbHurmmeqB4hb3TnY/2/4XnV2xkFv/ALAj91Ck9w4d+P8AVFse+Mgken8IM5RF76uRlrh2Dv8AZE7MA7Y91VcMpk+JrQe8QV1lCcsccecoEWPbEH9kRRrA4KlRuGuG1wEoW5pBhkccoEwyrSBGEtewzCJZdSMFUVAZJLgfJFElFxQBaZSWtahPXZ5Q1zQ7KkJoQus2uB6FAVdwaWknyT82z+gQNxYuMkiD0KtMloQFh649VW6O6jcNIcQVUVdEWbo1FBU1CWGHKdOoCsWqO9xokiqCg2mExoUBCkVHKTE6szAS2mzPCZUD347IREgktBEnCDrtR0hUPpjlNohCuo09EM9/cpjV6pc9okpG0Sl59D7IWs4DiR7K6qV4U5CChc+uQuU7p054Rz7AngT6mAh6lqAPHUDf/FoJP8JjsNt7nz/lH06zjxIHqkVrUpA43u9SB+Exp3oHDQB6Sf3QJxsMfSe7kmPQflRZS2Hl8HnMKTNRxG5Rq1d3VKiHBltS1a6HNLp9SURb3DY2uGfNB27izPI6yp1am/LeU6M3EqvmOB3NHh/CCZUJPdXV6rwIfwccqA2NALSB5BOgo7dVtoBRdt4mglLLi6ETEx3VNprzQQH+FUog1o0Qpt7JbfvBkRHKuqaxR2Fwe1JbjW6e10Hc6MBUomLZkdVpu3uhLXVHt+pvuE3rVNxJKHeArUqIcEz6Fe2wdghZ1zyx+2OuFtK7QBKzGtUJ8YxClo9Wa1YVZPlOKJWd02rK0FF6xaMAumco9gAA7lA0u6IpPj1SRMlYU9pCqLeB91I1MSen7qrfAJPqmQkV3FMQl9Sl0AR1WpEH/MoG8r4QaRKtjWCXeI/sP5Q77k/0wPb8oS6vQBniUI++bEgoo0Vexg+sTyUFVYM5THRNNNeHkw1ail8M0YyJKpRZM+SMT5y50HAwi7a4B8lrNQ0Wi3w7Y8xKxdxpxFQiSGTg9XeQ+/KeIlyexvRoOf8ASP8AZEgtp4+t/wCwSO518Ux8tnAw49z5Kqz11ky7koxZWal2Oaz3nJPsus3MZvmCeB3812wuadV+0OwBLivapdsguDhAgDIRiyZOPRCreFzILZ80A2mSZEhWXt5tpsgTwT7hAv1eB9PuqUTKUkFOaW/UkmrVGAzICCv/AIieSWkQkFxdF5klbRh9OafKvQXcaieG8KuhfOHOUApMMFaYowzkOWXQKm5yUhyvY9wWb4/hceX6fdnUmxxnukmtWm9jmtEmDlaPahLmidrsqZI9hO9HzfTbnaS0/UDlaO2uZWY1G1LKry2QJlGWd3POFlJWZqLTNfRr4RNJ8lIqFyIR1tXkTKiisBzIIjzUKv0oWlcQpVa4IhIzwZXcSXQOMIS64PXBRYeJ+yjUYJ9kDwZmX2pdIiQVbb6H0nlPGNY3opsq+IQJynYsWMNJtzTY1vYLQ2dZsZSphlgMQuU2k9cLRGcoqS2MrkNcTMQsN8cV2MZLYD/6T/aDiY7xK1zbHcfE4lvaVhvj6nTHhGXEiM8JkqK6TPnrnqVPumVLTQWyoiyhwnicqrRCsZNf8i2ceKlSAT5uzHs0AepSGpcnZtk8o7VLjcxjfNzj94/CSVXKkg6NFY3Rf4D0aI9Qu1bKo87WNjvOEksKzmuDhyF9M050sZUeBL2tO4cCe6XTFPLH8mWHwY57N737XHgdz2Suj8F1nNcS5jXz4GzIeOxjLT6hfWbZ7HuG5wLG+mT0RTdNY58gNeBnGIKtSZyqEu5H57rWr2OLXtIgkTBgkcweCubV901vRrZ9MsewEEmCDBaSZMEcZWIufgqj49lV+4yacgQCB9Lu6rIzk1H2YVrFJr4wnjfha52PeGAhhh0OE+rRyUsZZvfO1jiRyA04gwZ+xTsD7ux09eVF5mVmKdWvsJDYloDXbuD/AFGOqlY60abhSq/USIcc88Z9IWbR6nH5UJypM7qOmzk5kpZV0YzjAWvqN3CQqmUZ5WeJ2mNq21RmIlFULotGRn8LUusxHCGraW09EnAaoRt1JvdTZqLT1Vt9og2u24KUs+H3gGHcqcR0NRfBdfepQdNrN4yp/pKswjEKGbbnzV1HUWtMpNUs6u0nr2ygP0ldwwCPNGJEuujbs15oEKhnxGzdAcB5LCXzKrAckkiP5Se1e5ri98w3xR3d/SPvC0UbOSXIo+j61qnxKGNgEExmD17LD1C+5qF5mPuqNCsn3G50E5lx8zyt/pehtps48R5US0PKNaMo23IEQpOtZElPbmzcCcIK5aQMqDOjIarRiSkL8laPWHTMJTQsScwuiL0PBt0etKeE/wBMuqjYZuOzjb0Crs9NOMIurabXNIMINsMVsc0iRwTCf2WoN5DywwA4EAtMDnCR2zJaEPfEsMjCS7JlCMkaSrby9r8vHRhPhPdzZVVdrOYMtkxjEJSzWHkNG6IbtHpn+VVU1mXguaBjaSJyOFSPP5fEydhF28S1rXlocC4juBOTHZDWlHc/a0wXZe4ATjv/AJ1UrSy+bUaxjgBuk9S1pzgn3+60RthRBdTYHuzJOScnlUSuPFUe/TtcILgOh57ZQN7p7HOBiTiCccd0Y2q3JImPyqH3gceOIPscIkjyOKbW4ltvckPLCIwImOY6dwmLR5pM5jHguPIA8QMkAdvuidLvw7cH/wBJ2z3gwCoPovF8lSjjLtDMtypmmrBGIUpVHVkwR1JQNAI0rjgiilNgXyB2UHW47I2FEhKilNgfyR2VT7ZpEQmBC8aaKHn9M7V0VjpnKR3+gU9sE8GcHK3TqaBr6Y1zg49OR3SomUYyQv8AgCi0h7AMNj9ytybWULo2nspN8LQ0u8RgflOGlQ0mzzpyp6FVbTAfVYn4so/LPkvpTnL5v8f1N1ZlOORPtKMUXwtylTMdYWZqPJOWytBT0xqK060DGgAJg2mqo9CMUkAMtoSzVKcFp81ozTSXWGiAfNNC5P5GNnSloI4VGq2hLceqY6U0bGxwirmjLSO4RRyKZhi+JCDq1eiZ31rtcZ/zzCW17bOD7osdldO6c36XEHuCmFLXqrTM+wSb6XQVe8J2FJmkt9XlowSBgwhNXvSWB9OIMiJPhjmY65XdQolocWMcxpkB7RAHkegSawccgyfuZ6cd1XZ5kvHjB6HenXxbuBO4BzsgfVB/H8py9xe0FvhGD5+kD1Wfq2mxzBuJBy5sZjGJHumttdgPbPhHAxjvEdv5SZm9bj2OdM1IlwY4QRwT1Ttj1ktXLtvzRh4jaB2Hkivh7Ww8bXwHDlCPU8fnXJGn2aYrxXWkGF0nlM2sg4KGxWrjhiUDTIwubVKZXoQOyBauiliVIhcJcMKZJ1oLYfSriBJVxuQkhomDJPsrjUAAAmQFnTRjLhTehvvlK7/SqVRwNRu4j6XZBHuo0Lp3JGP3RL60iPJFMn/OUXoCfobAPCT6HKD/AEsYOCmTahGAcqT3NdyPEqVmsZTj3sUvows9rTZj0WsrsHRZDV3u3FsSAqRc5fkbaMw7Gge6dfKwlPw9T4OYI6rQbUzjsyus245joVmKrBBA+rkditdrn9QHKzJtyTxnt5d1EmPIzr2EkmFfTeCNplMrljWNMjJMoalsM4ycz6dE4uzSMjVar/27/ULK/D31H1XV5Wujm5+2OX8H1P5Q1x9XuP8A5Xl5ScY4q/SPQfgJLY/9V/oPyV5eTNvD/s+g2f0j0RDV5eTR6rOlSfwF5eQL4VhTC8vIGzxUHdF5eQIkqRyV5eUDRYf9Fw8ry8mBF3I/zqoheXkDIu5KQX31PXV5CJ5P5Gui/QPRNyvLyZyMzOsfWfRKKn1j0P4C8vLKXYkKNT/lL6XX0K8vK4dGsT//2Q==",
                "PrietaPuño",
                "Roedores",
                "user@animalfind.es",
                "Disponible",
                "Simple rata que le gusta apretar el puño cuando tiene rabia, aconsejable no molestar, por bien de la salud de uno mismo."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Conejo",
                "https://t2.uc.ltmcdn.com/es/posts/7/8/2/como_cuidar_un_conejo_domestico_7287_orig.jpg",
                "Griffith",
                "Roedores",
                "admin@animalfind.es",
                "Disponible",
                "El conejo común o conejo europeo es una especie de mamífero lagomorfo de la familia Leporidae, y el único miembro actual del género Oryctolagus. Mide hasta 50 cm y su masa puede ser hasta 2.5 kilogramos. Ha sido introducido en varios continentes y es la especie que se utiliza en la cocina y en la cunicultura."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Caballo", "https://okdiario.com/img/2021/04/13/-que-sabes-del-caballo-maremmano_-655x368.jpg",
                "Galopante",
                "Otros",
                "bryan@animalfind.es",
                "Disponible",
                "El caballo \u200B\u200B es un mamífero perisodáctilo domesticado de la familia de los équidos. Es un herbívoro perisodáctilo de gran porte, y cuello largo y arqueado poblado por largas crines. La mayoría de los animales que pastan tienen la pata hendida, es decir, sus patas terminan en dos dedos con casco."
        );

        crearAnimal(userRepository, animalTypeRepository, animalRepository,
                "Vaca",
                "https://images.ecestaticos.com/C0LO7pQz-ob4AdJO9gCT0GU5jiI=/0x0:1000x750/1200x900/filters:fill(white):format(jpg)/f.elconfidencial.com%2Foriginal%2Ff6d%2F1ba%2F967%2Ff6d1ba96704a71bf760887d64feafc85.jpg",
                "Querie",
                "Otros",
                "user@animalfind.es",
                "Disponible",
                "La vaca, en el caso de la hembra, o toro, en el caso del macho, es un mamífero artiodáctilo de la familia de los bóvidos. Anteriormente era considerado una subespecie de Bos primigenius, pero un estudio reciente lo considera una especie distinta."
        );
    }
        private void crearAnimal(UserRepository userRepository, AnimalTypeRepository animalTypeRepository, AnimalRepository animalRepository,
        String breed, String imageProfile, String name, String type, String userEmail, String status, String description) {
            Animal animal1 = new Animal();
            animal1.setBreed(breed);
            animal1.setName(name);
            animal1.setAge((int)(Math. random()*11));
            animal1.setImagenProfile(imageProfile);
            animal1.setDescription(description);
            animal1.setAnimal_type(animalTypeRepository.findByName(type));
            animal1.setUser(userRepository.findByEmail(userEmail));
            animal1.setStatus(status);
            animalRepository.save(animal1);
        }

}