package es.bgonzalez.animalfind.data.base;

import java.util.Objects;
import javax.persistence.*;

/**
 * Entidad base que contiene los campos en común de las entidades del modelo.
 */
@MappedSuperclass
public abstract class AbstractEntity {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Método que devuelve hashCode de la entidad.
     * @return valor del hashCode.
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**
     * Método que calcula si un objeto es igual que otro.
     * @param obj objeto a comparar.
     * @return resultado de la comparación.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AbstractEntity)) {
            return false; // null or other class
        }
        AbstractEntity other = (AbstractEntity) obj;

        if (id != null) {
            return id.equals(other.id);
        }
        return super.equals(other);
    }

    /**
     * Método que devuelve de forma leíble el valor del objeto.
     * @return forma leíble del objeto.
     */
    @Override
    public String toString() {
        String identifier = (id == null) ? "new" : String.valueOf(id);
        return String.format("%s#%s", getClass().getSimpleName(), identifier);
    }
}
