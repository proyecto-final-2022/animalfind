package es.bgonzalez.animalfind.data.entity;

import es.bgonzalez.animalfind.data.base.AbstractEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "animal_type", schema = "animalfind")
public class AnimalType extends AbstractEntity {
    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "animal_type", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Animal> animals = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(Set<Animal> animals) {
        this.animals = animals;
    }
}
