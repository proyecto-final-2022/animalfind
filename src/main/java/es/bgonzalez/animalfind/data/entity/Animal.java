package es.bgonzalez.animalfind.data.entity;

import es.bgonzalez.animalfind.data.base.AbstractEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Clase de un animal.
 */
@Entity
@Table(name = "animal", schema = "animalfind")
public class Animal extends AbstractEntity {
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "animaltype_id", nullable = false)
    private AnimalType animal_type;
    @Column(name = "breed", nullable = false)
    private String breed;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "age", nullable = false, length = 2)
    private Integer age;
    @Column(name = "description", nullable = false, length = 1000)
    private String description;
    @Column(name = "imagenProfile", nullable = false, length = 9000)
    private String imagenProfile;
    @Column(name = "status", nullable = false)
    private String status;
    @OneToMany(mappedBy = "animal", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Save> saves = new HashSet<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AnimalType getAnimal_type() {
        return animal_type;
    }

    public void setAnimal_type(AnimalType animal_type) {
        this.animal_type = animal_type;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagenProfile() {
        return imagenProfile;
    }

    public void setImagenProfile(String imagenProfile) {
        this.imagenProfile = imagenProfile;
    }

    public Set<Save> getSaves() {
        return saves;
    }

    public void setSaves(Set<Save> saves) {
        this.saves = saves;
    }
}