package es.bgonzalez.es.views.pages.user;

import es.bgonzalez.animalfind.data.entity.User;
import es.bgonzalez.animalfind.views.pages.user.profile.EditProfileForm;
import es.bgonzalez.animalfind.views.pages.user.profile.EditProfileFormBinder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProfileTest {
    private User testUser;
    private EditProfileForm editProfileForm;
    private EditProfileFormBinder editProfileFormBinder;

    @Before
    public void setupData(){
        testUser = new User();
        editProfileForm = new EditProfileForm();
        editProfileFormBinder = new EditProfileFormBinder(editProfileForm);

        testUser.setUsername("test");
        testUser.setEmail("test@test.com");
        testUser.setHashedPassword("test");
        testUser.setPhone("+34 111222333");
        testUser.setProfilePictureUrl("test");

        editProfileFormBinder.getEditProfileForm().getUsername().setValue(testUser.getUsername());
        editProfileFormBinder.getEditProfileForm().getEmail().setValue(testUser.getEmail());
        editProfileFormBinder.getEditProfileForm().getPassword().setValue(testUser.getHashedPassword());
        editProfileFormBinder.getEditProfileForm().getConfirmPassword().setValue(testUser.getHashedPassword());
        editProfileFormBinder.getEditProfileForm().getPhone().setValue(testUser.getPhone());
    }

    @Test
    public void FieldsForm(){
        Assert.assertEquals(editProfileFormBinder.getEditProfileForm().getUsername().getValue(), testUser.getUsername());
        Assert.assertEquals(editProfileFormBinder.getEditProfileForm().getEmail().getValue(), testUser.getEmail());
        Assert.assertEquals(editProfileFormBinder.getEditProfileForm().getPassword().getValue(), testUser.getHashedPassword());
        Assert.assertEquals(editProfileFormBinder.getEditProfileForm().getConfirmPassword().getValue(), testUser.getHashedPassword());
        Assert.assertEquals(editProfileFormBinder.getEditProfileForm().getPhone().getValue(), testUser.getPhone());
    }
}
