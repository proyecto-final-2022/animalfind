package es.bgonzalez.es.views.pages.user;

import es.bgonzalez.animalfind.data.entity.Animal;
import es.bgonzalez.animalfind.data.entity.AnimalType;
import es.bgonzalez.animalfind.data.entity.User;
import es.bgonzalez.animalfind.views.pages.user.animals.AnimalForm;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class MyAnimalTest {
    private User testUser;
    private List<AnimalType> animalTypesTests;
    private AnimalType animalTypeTest;
    private Animal animalTest;
    private AnimalForm animalForm;

    @Before
    public void setupData(){
        animalTypesTests = new ArrayList<>();
        testUser = new User();
        animalTypeTest = new AnimalType();
        animalTest = new Animal();
        animalForm = new AnimalForm(animalTypesTests);

        testUser.setUsername("test");
        testUser.setEmail("test@test.com");
        testUser.setHashedPassword("test");
        testUser.setPhone("+34 111222333");
        testUser.setProfilePictureUrl("test");

        animalTypeTest.setName("Perro");

        animalTypesTests.add(animalTypeTest);

        animalTest.setUser(testUser);;
        animalTest.setAnimal_type(animalTypeTest);
        animalTest.setBreed("Rottweiler");
        animalTest.setName("testAnimal");
        animalTest.setAge(1);
        animalTest.setDescription("test test test");
        animalTest.setStatus("Disponible");
        animalTest.setImagenProfile("test.jpg");

        animalForm.setAnimal(animalTest);
        animalForm.getAnimalType().setValue(animalTest.getAnimal_type());
        animalForm.getBreed().setValue(animalTest.getBreed());
        animalForm.getName().setValue(animalTest.getName());
        animalForm.getAge().setValue(animalTest.getAge());
        animalForm.getDescription().setValue(animalTest.getDescription());
        animalForm.getStatus().setValue(animalTest.getStatus());
        animalForm.getImagenProfile().setValue(animalTest.getImagenProfile());
    }

    @Test
    public void FieldsForm() {
        Assert.assertEquals(animalForm.getAnimalType().getValue(), animalTest.getAnimal_type());
        Assert.assertEquals(animalForm.getBreed().getValue(), animalTest.getBreed());
        Assert.assertEquals(animalForm.getName().getValue(), animalTest.getName());
        Assert.assertEquals(animalForm.getAge().getValue(), animalTest.getAge());
        Assert.assertEquals(animalForm.getDescription().getValue(), animalTest.getDescription());
        Assert.assertEquals(animalForm.getStatus().getValue(), animalTest.getStatus());
        Assert.assertEquals(animalForm.getImagenProfile().getValue(), animalTest.getImagenProfile());
    }

    @Test
    public void saveEventHasCorrectValues() {
        AtomicReference<Animal> savedAnimalRef = new AtomicReference<>(null);
        animalForm.addListener(AnimalForm.SaveEvent.class, e -> {
            savedAnimalRef.set(e.getAnimal());
        });
        animalForm.getSave().click();

        Animal savedAnimal = savedAnimalRef.get();

        Assert.assertEquals(animalTest.getAnimal_type(), savedAnimal.getAnimal_type());
        Assert.assertEquals(animalTest.getBreed(), savedAnimal.getBreed());
        Assert.assertEquals(animalTest.getName(), savedAnimal.getName());
        Assert.assertEquals(animalTest.getAge(), savedAnimal.getAge());
        Assert.assertEquals(animalTest.getDescription(), savedAnimal.getDescription());
        Assert.assertEquals(animalTest.getStatus(), savedAnimal.getStatus());
        Assert.assertEquals(animalTest.getImagenProfile(), savedAnimal.getImagenProfile());
    }

    @Test
    public void deleteEventHasCorrectValues() {
        AtomicReference<Animal> savedAnimalRef = new AtomicReference<>(null);
        animalForm.addListener(AnimalForm.SaveEvent.class, e -> {
            savedAnimalRef.set(e.getAnimal());
        });

        animalForm.getDelete().click();

        Animal deleteAnimal = savedAnimalRef.get();

        Assert.assertNull(deleteAnimal);
    }
}
