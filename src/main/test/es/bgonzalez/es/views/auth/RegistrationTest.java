package es.bgonzalez.es.views.auth;

import es.bgonzalez.animalfind.data.entity.User;
import es.bgonzalez.animalfind.views.auth.register.RegistrationForm;
import es.bgonzalez.animalfind.views.auth.register.RegistrationFormBinder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RegistrationTest {
    private User testUser;
    private RegistrationForm registrationForm;
    private RegistrationFormBinder registrationFormBinder;

    @Before
    public void setupData(){
        testUser = new User();
        registrationForm = new RegistrationForm();
        registrationFormBinder = new RegistrationFormBinder(registrationForm);

        testUser.setUsername("test");
        testUser.setEmail("test@test.com");
        testUser.setHashedPassword("test");
        testUser.setPhone("+34 111222333");
        testUser.setProfilePictureUrl("test");

        registrationFormBinder.getRegistrationForm().getUsername().setValue(testUser.getUsername());
        registrationFormBinder.getRegistrationForm().getEmail().setValue(testUser.getEmail());
        registrationFormBinder.getRegistrationForm().getPassword().setValue(testUser.getHashedPassword());
        registrationFormBinder.getRegistrationForm().getConfirmPassword().setValue(testUser.getHashedPassword());
        registrationFormBinder.getRegistrationForm().getPhone().setValue(testUser.getPhone());
    }

    @Test
    public void FieldsForm(){
        Assert.assertEquals(registrationFormBinder.getRegistrationForm().getUsername().getValue(), testUser.getUsername());
        Assert.assertEquals(registrationFormBinder.getRegistrationForm().getEmail().getValue(), testUser.getEmail());
        Assert.assertEquals(registrationFormBinder.getRegistrationForm().getPassword().getValue(), testUser.getHashedPassword());
        Assert.assertEquals(registrationFormBinder.getRegistrationForm().getConfirmPassword().getValue(), testUser.getHashedPassword());
        Assert.assertEquals(registrationFormBinder.getRegistrationForm().getPhone().getValue(), testUser.getPhone());
    }

}
